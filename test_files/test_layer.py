import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def visualize_voxels_model_and_ROI(voxels_ROI):
    # Create plot
    fig = plt.figure()
    ax = fig.gca(projection=Axes3D.name)
    # voxels_model = voxels.transpose(2, 0, 1)
    voxels_ROI = voxels_ROI.transpose(2, 0, 1)

    # ax.voxels(voxels_model, edgecolor="k")
    ax.voxels(voxels_ROI, edgecolor="red")

    ax.set_xlabel("Z")
    ax.set_ylabel("X")
    ax.set_zlabel("Y")

    ax.view_init(elev=15, azim=85)
    plt.show()
    # plt.close(fig)


def visualize_voxels_2D_ROI(size, ROI_2D, height):
    voxels_grid = np.zeros([size, size, size])

    for voxel in ROI_2D:
        j, l = tuple(voxel)
        j = int(j)
        k = int(height)
        l = int(l)
        voxels_grid[j, k, l] = 1

    visualize_voxels_model_and_ROI(voxels_grid)


def get_extent(size, free_space_core_cells, occupancy_distances):
    axis_boundry_min = np.any(free_space_core_cells == 0, axis=0)
    axis_boundry_max = np.any(free_space_core_cells == size - 1, axis=0)

    core_min_coords = tuple(np.amin(free_space_core_cells, axis=0))
    core_max_coords = tuple(np.amax(free_space_core_cells, axis=0))

    # proba --> SIGURNO NE VALJA !!!
    # y_max = np.max(free_space_core_cells[:, 1])
    # upper_layer = free_space_core_cells[np.where(free_space_core_cells[:, 1] == y_max)]

    # x_min = np.min(upper_layer[:, 0])
    # core_min = upper_layer[np.where(upper_layer[:, 0] == x_min)]
    # z_min = np.min(core_min[:, 2])

    # x_max = np.max(upper_layer[:, 0])
    # core_max = upper_layer[np.where(upper_layer[:, 0] == x_max)]
    # z_max = np.max(core_max[:, 2])

    # core_min_coords = tuple([x_min, y_max, z_min])
    # core_max_coords = tuple([x_max, y_max, z_max])

    # proba 2
    # coord = tuple(free_space_core_cells[0])
    # extent = np.ones([3, 2])
    # extent[:, 0] = core_min_coords - (occupancy_distances[coord] - 1) * ~axis_boundry_min

    # if np.any(extent[:, 0] < 0):
    #     extent[:, 0] = np.where(extent[:, 0] < 0, 0, extent[:, 0])

    # extent[:, 1] = core_max_coords + (occupancy_distances[coord] - 1) * ~axis_boundry_max

    # if np.any(extent[:, 1] >= size):
    #     extent[:, 1] = np.where(extent[:, 1] >= size, (size - 1), extent[:, 1])

    # original
    extent = np.ones([3, 2])
    extent[:, 0] = core_min_coords - (occupancy_distances[core_min_coords] - 1) * ~axis_boundry_min

    if np.any(extent[:, 0] < 0):
        extent[:, 0] = np.where(extent[:, 0] < 0, 0, extent[:, 0])

    extent[:, 1] = core_max_coords + (occupancy_distances[core_max_coords] - 1) * ~axis_boundry_max

    if np.any(extent[:, 1] >= size):
        extent[:, 1] = np.where(extent[:, 1] >= size, (size - 1), extent[:, 1])

    return extent


size = 32
height = 10
occupancy = 5

slice_layer = np.zeros([size, size], dtype=int)

"------------  2D  ------------"
## EXAMPLE 1
# slice_layer[12:21, 10:18] = 1
# slice_layer[20, 15:17] = 0
# slice_layer[12, 10:13] = 0

# EXAMPLE 2
slice_layer[10:13, 0:4] = 1
slice_layer[12, 1:3] = 0
slice_layer_3D = np.zeros([size, size, size], dtype=int)
slice_layer_3D[10:13, height, 0:4] = 1
slice_layer_3D[12, height, 1:3] = 0

occupancy_distances = np.zeros([size, size, size], dtype=int) * -1
occupancy_distances[10:13, height, 0:4] = occupancy
occupancy_distances[12, height, 1:3] = 0

## EXAMPLE 3
# slice_layer[10:14, 10:15] = 1
# slice_layer[10, 10:13] = 0
# slice_layer[11, 10] = 0

# slice_layer_3D = np.zeros([size, size, size], dtype=int)
# slice_layer_3D[10:14, height, 10:15] = 1
# slice_layer_3D[10, height, 10:13] = 0
# slice_layer_3D[11, height, 10] = 0

# occupancy_distances = np.zeros([size, size, size], dtype=int) * -1
# occupancy_distances[10:14, height, 10:15] = occupancy
# occupancy_distances[10, height, 10:13] = 0
# occupancy_distances[11, height, 10] = 0


# plt.imshow(slice_layer, interpolation="none")
# plt.show()
free_space_core_slice_layer = np.argwhere(slice_layer == 1)
visualize_voxels_2D_ROI(size, free_space_core_slice_layer, height)

slice_tuples = np.argwhere(slice_layer_3D == 1)
# visualize_voxels_model_and_ROI(slice_layer_3D)


## ORIGINAL
" Check if this slice is squared shaped"
j, j_counts = np.unique(free_space_core_slice_layer[:, 0], return_counts=True)
k, k_counts = np.unique(free_space_core_slice_layer[:, 1], return_counts=True)

if (j.size * k.size) > len(free_space_core_slice_layer):
    stop = 1

    free_space_core_slice_layer_adjusted = []
    if j.size < k.size:
        k_core = k[np.where(k_counts != min(k_counts))]
        if k_core.size == 0:
            print("k_core.size == 0")
        for k_ in k_core:
            section = free_space_core_slice_layer[:, 1] == k_
            free_space_core_slice_layer_adjusted.append(free_space_core_slice_layer[section, :])
            stop = 1
    elif j.size > k.size:
        j_core = j[np.where(j_counts != min(j_counts))]
        if j_core.size == 0:
            print("j_core.size == 0")
        for j_ in j_core:
            section = free_space_core_slice_layer[:, 0] == j_
            free_space_core_slice_layer_adjusted.append(free_space_core_slice_layer[section, :])
            stop = 1
    else:
        # TODO: provjeriti je li ovo ispravno kao odluka za dalje?
        print("...else...")

    free_space_core_slice_layer_adjusted = np.vstack(free_space_core_slice_layer_adjusted)
    visualize_voxels_2D_ROI(size, free_space_core_slice_layer_adjusted, height)

# return back to 3D
free_space_core_slice_layer_adjusted = np.vstack(free_space_core_slice_layer_adjusted)
iAxis = 1
jAxis = iAxis - 1
kAxis = iAxis - 2

if np.logical_and(jAxis < 0, kAxis < 0):
    tmp = jAxis
    jAxis = kAxis
    kAxis = tmp

free_space_core_slice_adjusted = []
for row in free_space_core_slice_layer_adjusted:
    section = (slice_tuples[:, jAxis] == row[0]) * (slice_tuples[:, kAxis] == row[1])
    free_space_core_slice_adjusted.append(slice_tuples[section])
    stop = 1

free_space_core_slice_v1 = np.vstack(free_space_core_slice_adjusted)


stop = 1

## MATEJA NEW -v1
# " Check if this slice is squared shaped"
slice_min_coords = tuple(np.amin(free_space_core_slice_layer, axis=0))
slice_max_coords = tuple(np.amax(free_space_core_slice_layer, axis=0))

free_space_core_slice_layer_adjusted = []
for i in range(slice_min_coords[0], slice_max_coords[0] + 1, 1):
    for j in range(slice_min_coords[1], slice_max_coords[1] + 1, 1):
        free_space_core_slice_layer_adjusted.append(tuple([i, j]))

visualize_voxels_2D_ROI(size, free_space_core_slice_layer_adjusted, height)

# return back to 3D
free_space_core_slice_layer_adjusted = np.vstack(free_space_core_slice_layer_adjusted)
iAxis = 1
jAxis = iAxis - 1
kAxis = iAxis - 2

if np.logical_and(jAxis < 0, kAxis < 0):
    tmp = jAxis
    jAxis = kAxis
    kAxis = tmp

free_space_core_slice_adjusted = []
for row in free_space_core_slice_layer_adjusted:
    section = (slice_tuples[:, jAxis] == row[0]) * (slice_tuples[:, kAxis] == row[1])
    free_space_core_slice_adjusted.append(slice_tuples[section])
    stop = 1

free_space_core_slice_v2 = np.vstack(free_space_core_slice_adjusted)
stop = 1

## end - MATEJA NEW -v1

## MATEJA NEW - v2
jAxis = iAxis - 1
kAxis = iAxis - 2

if np.logical_and(jAxis < 0, kAxis < 0):
    tmp = jAxis
    jAxis = kAxis
    kAxis = tmp

slice_min_coords = tuple(np.amin(free_space_core_slice_layer, axis=0))
slice_max_coords = tuple(np.amax(free_space_core_slice_layer, axis=0))

free_space_core_slice_layer_adjusted = []
for j in range(slice_min_coords[0], slice_max_coords[0] + 1, 1):
    for k in range(slice_min_coords[1], slice_max_coords[1] + 1, 1):
        item = np.zeros([3], dtype=int)
        item[iAxis] = iAxis_value
        item[jAxis] = j
        item[kAxis] = k
        free_space_core_slice_layer_adjusted.append(item)

# end - MATEJA NEW - v2


extent_v1 = get_extent(size, free_space_core_slice_v1, occupancy_distances)
extent_v2 = get_extent(size, free_space_core_slice_v2, occupancy_distances)

stop = 1
