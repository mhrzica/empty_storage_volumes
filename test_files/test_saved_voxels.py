import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pathlib import Path
import os

import sys

sys.path.insert(0, "")
from Shapenet.BinvoxModel import BinvoxModel


def visualize_voxels_model_and_ROI(voxels_model, voxels_ROI):
    # Create plot
    fig = plt.figure()
    ax = fig.gca(projection=Axes3D.name)
    voxels_model = voxels_model.transpose(2, 0, 1)
    voxels_ROI = voxels_ROI.transpose(2, 0, 1)

    ax.voxels(voxels_model, edgecolor="k")
    ax.voxels(voxels_ROI, edgecolor="red")

    ax.set_xlabel("Z")
    ax.set_ylabel("X")
    ax.set_zlabel("Y")

    ax.view_init(elev=15, azim=85)
    plt.show()
    # plt.close(fig)


grid_size = 32
root = "/media/mateja/ubuntu_storage/vizualizacija/free_space_core/voxels_txt/"

model_1 = "8d0d7787f4babee7e66285d36ebb986"
model_2 = "e3738af2b535ea36ecf5e8796652a83c"
model_3 = "fc7a8af42911dc77b4169e4cad9bb63a"

path = root + model_3

binvox_path = Path(path + ".binvox/")
voxelized_model = BinvoxModel.read_binvox_file(binvox_path)


fsc_path = Path(path + "_fsc.txt")
loaded_fsc = np.loadtxt(fsc_path)
load_original_fsc = loaded_fsc.reshape(loaded_fsc.shape[0], loaded_fsc.shape[1] // grid_size, grid_size)
visualize_voxels_model_and_ROI(voxelized_model.voxels, load_original_fsc)


fsc_slice_path = Path(path + "_fsc_slice.txt")
loaded_fsc_slice = np.loadtxt(fsc_slice_path)
load_original_fsc_slice = loaded_fsc_slice.reshape(
    loaded_fsc_slice.shape[0], loaded_fsc_slice.shape[1] // grid_size, grid_size
)
visualize_voxels_model_and_ROI(voxelized_model.voxels, load_original_fsc_slice)

check = np.array_equal(load_original_fsc, load_original_fsc_slice)

fsc_slice_adjusted = Path(path + "_fsc_slice_adjusted.txt")

if Path(fsc_slice_adjusted).is_file():
    loaded_fsc_slice_adjusted = np.loadtxt(fsc_slice_adjusted)
    load_original_fsc_slice_adjusted = loaded_fsc_slice_adjusted.reshape(
        loaded_fsc_slice_adjusted.shape[0], loaded_fsc_slice_adjusted.shape[1] // grid_size, grid_size
    )
    visualize_voxels_model_and_ROI(voxelized_model.voxels, load_original_fsc_slice_adjusted)
else:
    print("\n\n\n!!! Model does not have adjusted FSC slice !!!")


stop = 1
