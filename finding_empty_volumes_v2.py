#  Copyright (C) 2021 Mateja Hržica
import argparse
import logging
import math
import os
import sys

from Shapenet.BinvoxModel import BinvoxModel
from Shapenet.Dataset import Dataset
from Shapenet.MeshModel import MeshModel


" TODO: Mijenjala sam parametre: model_metric_size, hist_avg_threshold, flat_surface_threshold "


def parse_arguments():
    # Arguments
    parser = argparse.ArgumentParser(description="Display binvox file.")

    parser.add_argument("-root", "--dataset_path", type=str, help="Path to dataset.")
    # parser.add_argument(
    #     "-save_vis",
    #     "--visualization_folder_path",
    #     type=str,
    #     help="Path to folder where the images of the model with all bounding boxes will be saved.",
    # )
    parser.add_argument(
        "-mdl_bb", "--model_bb_normalization", default=0.5, help="Normalize CAD model to [-0.5, 0.5]^3 cube",
    )
    parser.add_argument(
        "-mdl_size", "--model_metric_size", default=1, help="Maximal (metric) dimension of the CAD model",
    )
    parser.add_argument(
        "-wlh",
        "--storage_size_threshold",
        # default=0.085,
        default=0.05,
        help="Minimal (metric) dimension of the storage volume candidate (w x l x h)",
    )
    parser.add_argument(
        "-f",
        "--flat_surface_threshold",
        # default=0.25,
        default=0,
        help="Minimal ratio of occupied voxels to the total area of the storage volume candidate underside",
    )
    parser.add_argument(
        "-extend",
        "--extend_bbox_percentage",
        # default=0.1,
        default=0.0,
        help="",
    )
    parser.add_argument(
        "-hist_bins",
        "--histogram_bins_width",
        # default=0.005,
        default=0.003,
        # default=0.1,
        help="",
    )
    parser.add_argument(
        "-hist_trsh",
        "--histogram_avg_threshold",
        default=2.5,
        # default=3,
        help="",
    )

    args = parser.parse_args()
    return args


def main(config):
    logging.info("\n\n _________ FINDING EMPTY VOLUMES _________\n ")

    classes_list = os.listdir(configuration.dataset_path)

    for iClass in classes_list:
        print("Class name: ", iClass, "\n")

        # if iClass != "Table":
        #     continue

        dataset = Dataset(config.dataset_path, iClass)

        for iModel in sorted(dataset.models_list, reverse=False):

            # if iModel != "e3738af2b535ea36ecf5e8796652a83c":
            # if iModel != "8d0d7787f4babee7e66285d36ebb986":
            # if iModel != "fc7a8af42911dc77b4169e4cad9bb63a":

            # if iModel != "37ed10ebcb9c0cbc932ec7aa2da5961e":
            if iModel != "24c62fe437ecf586d42b9650f19dd425":
                # if iModel != "1b35d8bb3a01668f6fa63795f94c4d8c":
                continue

            print("Model name: ", iModel, "\n")

            binvox_path = dataset.get_voxelized_model_path(iModel)
            voxelized_model = BinvoxModel.read_binvox_file(binvox_path)

            bounding_box_size_threshold = math.ceil(
                config.storage_size_threshold / (config.model_metric_size / voxelized_model.size)
            )

            voxelized_model.detect_storage_volumes(bounding_box_size_threshold, configuration.flat_surface_threshold)

            if len(voxelized_model.storage_volumes) == 0:
                print("\tNo voxelized storage volume detected.")
                continue

            off_path = dataset.get_mesh_model_path(iModel)
            mesh_model = MeshModel(configuration.model_bb_normalization)
            mesh_model.load_mesh_off_file(off_path)
            mesh_model.import_voxelized_bboxes(voxelized_model.storage_volumes)
            mesh_model.transform_voxelized_storage_volumes(
                config.extend_bbox_percentage, config.histogram_bins_width, config.histogram_avg_threshold
            )

            # mesh_model.chech_storage_size(config.storage_size_threshold)
            if len(mesh_model.storage_volumes) == 0:
                print("\tNo storage volume detected.")
                continue

            # mesh_model.remove_noise_bboxes()
            # mesh_model.recheck_supporting_surface()

            filePath_visualization = dataset.get_visualization_path(iModel)
            # mesh_model.visualize_bboxes(iModel, bSave=False, folderPath=filePath_visualization)

            # bbox_color_ = [1, 0.5, 0]
            # bbox_color_ = [0, 0.5, 0.5]
            bbox_color_ = [1, 0, 1]

            mesh_model.visualize_bboxes_v2(iModel, bbox_line_width_px=7, bbox_color=bbox_color_, show_axes=False)

            filePath_bboxes = dataset.get_bbox_save_path(iModel)
            # mesh_model.save_bboxes_data_to_file(filePath_bboxes, iClass)

        print("\n\nCOMPLETE\n\n")

    return 0


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    configuration = parse_arguments()
    sys.exit(main(configuration))
