import os
from pathlib import Path
import sys
import logging
from matplotlib.colors import same_color
import matplotlib.pyplot as plt
import numpy as np
from numpy.core.fromnumeric import size
import scipy as sc
from scipy import ndimage
import cc3d
import open3d as o3d
import open3d.visualization.gui as gui
from scipy.ndimage.morphology import black_tophat
from skimage.feature import peak_local_max

from BinvoxModel import BinvoxModel
from finding_empty_volumes import o3d_visualization


def get_bounding_box_verteces(extent):

    """ Finds all vertices of the voxelized bounding box. 

    Parameters
    ---------
    bounding_box_coords : ndarray, shape(3, 2) 
        cooridnates (xyz) are written in rows
        first column contains minimal coordinates, second column contains maximal coordinates

    Returns
    -------
    self.vertices : ndarray, shape (8,3)
        coordinates of the voxelized bounding box vertices
    """

    vertices = np.zeros((8, 3))

    iPoint = 0
    for y in range(2):
        for x in range(2):
            for z in range(2):
                vertices[iPoint, 0] = extent[0, x]
                vertices[iPoint, 1] = extent[1, y]
                vertices[iPoint, 2] = extent[2, z]
                iPoint += 1

    return vertices


def scene_bbox_visualization(
    scene_name, scene, bboxes_list, bbox_line_width_px=5, bbox_color=[0, 0, 0], show_axes=False
):
    gui.Application.instance.initialize()

    vis = o3d.visualization.O3DVisualizer()
    vis.show_axes = show_axes
    vis.line_width = bbox_line_width_px

    for i, iBox in enumerate(bboxes_list):
        bbox_lineset = o3d.geometry.LineSet.create_from_axis_aligned_bounding_box(iBox)

        if i == 0:
            bbox_color = [1, 0.3, 0]
            # bbox_color = [1, 0, 0]
        if i == 1:
            bbox_color = [0, 1, 0]
        if i == 2:
            bbox_color = [0, 0, 1]

        colors = [bbox_color for i in range(len(bbox_lineset.lines))]
        bbox_lineset.colors = o3d.utility.Vector3dVector(colors)

        vis.add_geometry("bbox " + str(i), bbox_lineset)

    scene.paint_uniform_color([0.254, 0.254, 0.254])
    vis.add_geometry("scene", scene)

    vis.reset_camera_to_default()
    vis.show_settings = True

    print(scene_name)

    gui.Application.instance.add_window(vis)
    gui.Application.instance.run()

    stop = 1


class MeshModel:
    def __init__(self, model_normalization):
        # self.voxelized_bboxes = voxelized_bboxes
        self.model_normalization = model_normalization
        self.voxelized_bboxes = {}
        self.mesh = {}
        self.vertices = {}
        self.storage_volumes = []

    def load_mesh_off_file(self, file_path):
        """Loads mesh model with Open3D library

        Parameters
        ---------
        file_path : Absolute file path to mesh model file
        """
        try:
            self.mesh = o3d.io.read_triangle_mesh(file_path)
            self.mesh.compute_vertex_normals()
            self.mesh.compute_triangle_normals()
            self.vertices = np.asarray(self.mesh.vertices)

        except FileNotFoundError as exc:
            logging.error(f"Mesh model file {file_path} not found")
            raise Exception("Error happened. Could not find mesh model file.") from exc

    def import_voxelized_bboxes(self, voxelized_bboxes):
        self.voxelized_bboxes = voxelized_bboxes

    def transform_voxelized_storage_volumes(
        self,
        extend_bb_percentage,
        histogram_bins_width_metric,
        histogram_avg_threshold,
        bVisualize_SupportingSurface=False,
    ):
        print("\tAnnotating storage volumes in mesh model.\n")
        for i, iBox in enumerate(self.voxelized_bboxes):
            iStorageVolume = self._bbox(self, iBox)

            (
                iStorageVolume.point_cloud,
                iStorageVolume.axis_aligned_o3d,
                iStorageVolume.center,
                iStorageVolume.wlh,
                iStorageVolume.wlh_half,
                iStorageVolume.mesh,
                iStorageVolume.vertices,
            ) = iStorageVolume.transform_voxelized_bbox_to_mesh(iBox.vertices)

            bVisualize = False
            if bVisualize:
                scene_name = "furniture_model"
                bboxes_list = [iStorageVolume.axis_aligned_o3d]
                # scene_bbox_visualization(scene_name, self.mesh, bboxes_list, bbox_line_width_px=5)

                if np.logical_and(
                    np.logical_and(
                        iStorageVolume.voxelized_bbox.iFreeSpaceCore[0] == 6,
                        iStorageVolume.voxelized_bbox.iFreeSpaceCore[1] == 1,
                    ),
                    iStorageVolume.voxelized_bbox.iFreeSpaceCore[2] == 8,
                ):
                    bbox_file = np.zeros([8,])
                    bbox_file[0:3] = iStorageVolume.center
                    bbox_file[3:6] = iStorageVolume.wlh_half
                    stop = 1

                    scene_bbox_visualization(scene_name, self.mesh, bboxes_list, bbox_line_width_px=5)
                    stop = 1

            margin = 1
            histogram_min_range, histogram_max_range = iStorageVolume.get_voxel2mesh_histogram_range(
                iBox.extent, margin
            )
            iStorageVolume.dilate_to_include_furniture_wall_v2(
                histogram_min_range, histogram_max_range, histogram_bins_width_metric, histogram_avg_threshold
            )

            # old dilation in mesh
            # iStorageVolume.dilate_to_include_furniture_wall(
            #     extend_bb_percentage, histogram_bins_width_metric, histogram_avg_threshold
            # )

            bVisualize = False
            if bVisualize:
                scene_name = "furniture_model"
                bboxes_list = [iStorageVolume.axis_aligned_o3d]
                scene_bbox_visualization(scene_name, self.mesh, bboxes_list, bbox_line_width_px=5)

                if np.logical_and(
                    np.logical_and(
                        iStorageVolume.voxelized_bbox.iFreeSpaceCore[0] == 6,
                        iStorageVolume.voxelized_bbox.iFreeSpaceCore[1] == 1,
                    ),
                    iStorageVolume.voxelized_bbox.iFreeSpaceCore[2] == 8,
                ):
                    bbox_file = np.zeros([8,])
                    bbox_file[0:3] = iStorageVolume.center
                    bbox_file[3:6] = iStorageVolume.wlh_half

                    scene_bbox_visualization(scene_name, self.mesh, bboxes_list, bbox_line_width_px=5)
                    stop = 1

            if bVisualize_SupportingSurface:

                supporting_surface_vertices = iStorageVolume.voxelized_bbox.supporting_surface_vertices

                (
                    supporting_surface_pc,
                    supporting_surface_axis_aligned_o3d,
                    supporting_surface_center,
                    supporting_surface_wlh,
                    supporting_surface_wlh_half,
                    supporting_surface_mesh,
                    supporting_surface_vertices,
                ) = iStorageVolume.transform_voxelized_bbox_to_mesh(supporting_surface_vertices)

                o3d.visualization.draw_geometries(
                    [iStorageVolume.mesh_model.mesh, supporting_surface_axis_aligned_o3d], point_show_normal=True,
                )
                stop = 1

            self.storage_volumes.append(iStorageVolume)

    def chech_storage_size(self, storage_size_threshold):
        print("\tRecheck bboxes size.\n")

        storage_volumes_update = []
        for i, iBox in enumerate(self.storage_volumes):
            storage_volume_dimensions = np.subtract(iBox.extent[:, 1], iBox.extent[:, 0])
            if np.all(storage_volume_dimensions >= storage_size_threshold):
                storage_volumes_update.append(iBox)

        self.storage_volumes = storage_volumes_update

        print("Done!\n\n")

    def recheck_supporting_surface(self):
        remove_bboxes = []
        for i, iBox in enumerate(self.storage_volumes):

            mesh_array = np.asarray(self.mesh.vertices)
            limit = 0.01

            height_min = iBox.height * (1 - limit)
            height_max = iBox.height * (1 + limit)

            extended_bbox_vertices_interval = np.logical_and(
                mesh_array[:, 1] >= height_min, mesh_array[:, 1] <= height_max
            )

            if np.count_nonzero(extended_bbox_vertices_interval) <= 10:
                remove_bboxes.append(i)

            stop = 1

        remove_bboxes = np.unique(remove_bboxes)
        deleted = 0

        for element in remove_bboxes:
            del self.storage_volumes[element - deleted]
            deleted += 1

    def remove_noise_bboxes(self):
        extents_all = [x.extent for x in self.storage_volumes]
        heights_all = [x.height for x in self.storage_volumes]

        remove_bboxes = []

        for i, iHeight in enumerate(heights_all):
            height_neighborhood = np.logical_and(heights_all >= iHeight - 0.01, heights_all <= iHeight + 0.01)

            for j, in_neighborhood in enumerate(height_neighborhood):
                if i == j:
                    continue

                if in_neighborhood:
                    if np.logical_and(
                        extents_all[j][0, 0] - 0.01 <= extents_all[i][0, 0],
                        extents_all[j][0, 1] + 0.01 >= extents_all[i][0, 1],
                    ):
                        if np.logical_and(
                            extents_all[j][2, 0] - 0.01 <= extents_all[i][2, 0],
                            extents_all[j][2, 1] + 0.01 >= extents_all[i][2, 1],
                        ):
                            remove_bboxes.append(i)

                    if np.logical_and(
                        extents_all[i][0, 0] - 0.01 <= extents_all[j][0, 0],
                        extents_all[i][0, 1] + 0.01 >= extents_all[j][0, 1],
                    ):
                        if np.logical_and(
                            extents_all[i][2, 0] - 0.01 <= extents_all[j][2, 0],
                            extents_all[i][2, 1] + 0.01 >= extents_all[j][2, 1],
                        ):
                            remove_bboxes.append(j)

        remove_bboxes = np.unique(remove_bboxes)

        deleted = 0
        for element in remove_bboxes:
            del self.storage_volumes[element - deleted]
            deleted += 1

    def visualize_bboxes(self, model_name, bSave=True, folderPath=""):
        vis = o3d.visualization.Visualizer()
        vis.create_window(window_name=model_name)

        firstRun = True
        for iBox in self.storage_volumes:

            if firstRun:
                vis.add_geometry(self.mesh)
                vis.update_geometry(self.mesh)
                vis.poll_events()
                vis.update_renderer()
                firstRun = False

            vis.add_geometry(iBox.axis_aligned_o3d)
            vis.update_geometry(iBox.axis_aligned_o3d)
            vis.poll_events()
            vis.update_renderer()

        ctr = vis.get_view_control()
        ctr.set_lookat([0, 0, 0])
        ctr.set_front([1, 1, 1])
        ctr.set_up([0, 1, 0])

        vis.run()

        if bSave:
            # filePath = folderPath + model_name + ".png"
            # vis.capture_screen_image(filePath)
            vis.capture_screen_image(folderPath)

        vis.destroy_window()

    def visualize_bboxes_v2(self, scene_name, bbox_line_width_px=5, bbox_color=[1, 0.5, 0], show_axes=False):

        # o3d.visualization.draw_geometries([self.mesh])
        # stop = 1

        gui.Application.instance.initialize()

        vis = o3d.visualization.O3DVisualizer()
        vis.show_axes = show_axes
        vis.line_width = bbox_line_width_px

        for i, iBox in enumerate(self.storage_volumes):
            bbox_lineset = o3d.geometry.LineSet.create_from_axis_aligned_bounding_box(iBox.axis_aligned_o3d)

            colors = [bbox_color for i in range(len(bbox_lineset.lines))]
            bbox_lineset.colors = o3d.utility.Vector3dVector(colors)

            vis.add_geometry("bbox " + str(i), bbox_lineset)

        vis.add_geometry("scene", self.mesh)

        vis.reset_camera_to_default()
        vis.show_settings = True

        print(scene_name)

        gui.Application.instance.add_window(vis)
        gui.Application.instance.run()

        stop = 1

    def save_bboxes_data_to_file(self, file_path, iClass):
        """ _________ SAVE BOUNDING BOX COORIDNATES TO NPZ FILE _________ """
        # Shapenet (used for detecting BB on models) has coordinate system where Y is up, X is forward, Z is right-ward)
        # Votenet needs 3D bounding boxes as its center (x,y,z), size (l,w,h) and heading angle (along the up-axis; rotation radius from +X towards -Y; +X is 0 and -Y is pi/4).
        # Votenet has point clouds in the upright coordinate system (Z is up, Y is forward, X is right-ward)

        print("Saving bboxes to file.")
        if os.path.isdir(file_path):
            if os.listdir(file_path):
                print("Directory is not empty. Old files will be deleted.")
                [f.unlink() for f in Path(file_path).glob("*") if f.is_file()]

            for count, iBox in enumerate(self.storage_volumes):
                npz_file = "Storage_volume_{0}.npz".format(count)

                save_to = os.path.join(file_path, npz_file)
                np.savez(
                    os.path.join(file_path, npz_file),
                    model_class=iClass,
                    vertices=iBox.vertices,
                    center=iBox.center,
                    wlh_half=iBox.wlh_half,
                )

        else:
            print("Given directory doesn't exist")

    def createInner(self):
        return MeshModel._bbox(self)

    class _bbox(BinvoxModel._bbox):
        def __init__(self, mesh_model, voxelized_bbox):
            self.mesh_model = mesh_model
            self.voxelized_bbox = voxelized_bbox

            self.point_cloud = {}
            self.axis_aligned_o3d = {}
            self.mesh = {}
            self.vertices = {}
            self.center = {}
            self.height = {}

            # extent represents the minimum bounding rectangle (xmin, ymin,zmin and xmax, ymax, zmax)
            # wlh stands for width, length and height
            self.extent = np.empty([3, 2])
            self.wlh = {}
            self.wlh_half = {}

            self.supporting_surface = {}

        def transform_voxelized_bbox_to_mesh(self, bbox_vertices):
            vertices2convert = self.prepare_for_transformation_to_mesh(bbox_vertices)

            (
                bbox_point_cloud,
                bbox_axis_aligned_o3d,
                bbox_center,
                bbox_wlh,
                bbox_wlh_half,
                bbox_mesh,
                bbox_vertices,
            ) = self.transform_voxels2mesh(vertices2convert)

            return (
                bbox_point_cloud,
                bbox_axis_aligned_o3d,
                bbox_center,
                bbox_wlh,
                bbox_wlh_half,
                bbox_mesh,
                bbox_vertices,
            )

        def prepare_for_transformation_to_mesh(self, bbox_vertices):

            # Used for transformation from voxelized model reference framte to watertight mesh model reference frame in Shapenet
            # bbox_vertices = self.voxelized_bbox.vertices
            min_vertices = bbox_vertices.min(axis=0)
            max_vertices = bbox_vertices.max(axis=0)

            vertices2convert = np.copy(bbox_vertices)

            for i in range(3):
                mask_min = np.logical_and(bbox_vertices[:, i] == min_vertices[i], bbox_vertices[:, i] > 0)
                mask_max = np.logical_and(
                    bbox_vertices[:, i] == max_vertices[i],
                    bbox_vertices[:, i] < self.voxelized_bbox.voxelized_model.size - 1,
                )

                vertices2convert[:, i] = np.where(mask_min, vertices2convert[:, i] - 0.5, vertices2convert[:, i])

                vertices2convert[:, i] = np.where(mask_max, vertices2convert[:, i] + 0.5, vertices2convert[:, i])

            return vertices2convert

        def transform_voxels2mesh(self, vertices2convert):

            # def bounding_box_mesh_watertight(
            #     voxels_scale, voxels_translate, binvox_size, bounding_box_core_coords, model_bb_normalization
            # ):
            """ Transform BB from voxels to point cloud.
            Convert voxelized bounding box to open3d point cloud based on scaled watertight *.off CAD model from the Shapenet

            Parameters
            ---------
            voxels_scale: float
                parameter from binvox file
            voxels_translate: ndarray
                parameter from binvox file
            bounding_box_core_coords: ndarray, shape(8,3)
                coordinates of the voxelized bounding box vertices

            Returns
            -------
            bb_pcd: open3D geometry PointCloud
                bounding box vertices
                point cloud array of bounding box in CAD model reference frame

            bb_o3d: open3D geometry OrientedBoundingBox
                3D bounding box () in CAD model reference frame

            bb_mesh: open3D geometry TriangleMesh
                mesh of the 3D bounding box in CAD model reference frame
            """

            scale_binvox = 1 / self.voxelized_bbox.voxelized_model.size

            scale = self.voxelized_bbox.voxelized_model.scale * scale_binvox

            bbox_point_cloud = o3d.geometry.PointCloud()

            bbox_point_cloud.points = o3d.utility.Vector3dVector(vertices2convert)

            bbox_point_cloud.scale(scale, np.zeros((3, 1)))

            bb_translate_ = (
                -np.array(
                    [
                        self.mesh_model.model_normalization,
                        self.mesh_model.model_normalization,
                        self.mesh_model.model_normalization,
                    ]
                )
                * self.voxelized_bbox.voxelized_model.scale
            )

            voxels_translate = np.asarray(self.voxelized_bbox.voxelized_model.translate)
            bb_translate = np.add(bb_translate_, voxels_translate)
            bbox_point_cloud.translate(bb_translate)
            bbox_point_cloud.translate([scale / 2, scale / 2, scale / 2])

            bb_pcd_center = bbox_point_cloud.get_center()

            bbox_axis_aligned_o3d = o3d.geometry.AxisAlignedBoundingBox.create_from_points(bbox_point_cloud.points)
            axis_aligned_o3d_dimensions_max = bbox_axis_aligned_o3d.get_max_bound()
            axis_aligned_o3d_dimensions_min = bbox_axis_aligned_o3d.get_min_bound()
            axis_aligned_o3d_dimensions = np.subtract(axis_aligned_o3d_dimensions_max, axis_aligned_o3d_dimensions_min)

            bbox_center = bbox_axis_aligned_o3d.get_center()
            bbox_wlh = (
                bbox_axis_aligned_o3d.get_extent()
            )  # Extent in Open3D: Get the length of the bounding box in x, y, and z dimension.
            bbox_wlh_half = bbox_wlh / 2

            bbox_mesh = o3d.geometry.TriangleMesh.create_box(
                width=axis_aligned_o3d_dimensions[0],
                height=axis_aligned_o3d_dimensions[1],
                depth=axis_aligned_o3d_dimensions[2],
            )

            bbox_mesh.translate(bb_pcd_center, relative=False)

            bbox_mesh.paint_uniform_color([0, 0.5, 0.5])
            bbox_mesh.compute_vertex_normals()
            bbox_mesh.compute_triangle_normals()

            bbox_vertices = np.asarray(bbox_mesh.vertices)

            return (
                bbox_point_cloud,
                bbox_axis_aligned_o3d,
                bbox_center,
                bbox_wlh,
                bbox_wlh_half,
                bbox_mesh,
                bbox_vertices,
            )

        def update_bbox(self):
            self.point_cloud.points = o3d.utility.Vector3dVector(self.vertices)
            self.axis_aligned_o3d = o3d.geometry.AxisAlignedBoundingBox.create_from_points(self.point_cloud.points)
            self.axis_aligned_o3d.color = np.random.random_sample((3, 1))
            # self.axis_aligned_o3d.color = [1, 0, 0]

            self.center = self.axis_aligned_o3d.get_center()
            self.wlh = (
                self.axis_aligned_o3d.get_extent()
            )  # Extent in Open3D: Get the length of the bounding box in x, y, and z dimension.
            self.wlh_half = self.wlh / 2

        def get_voxel2mesh_histogram_range(self, voxel_bbox_extent, margin, bVisualize=False):
            extent_dilated = np.copy(voxel_bbox_extent).astype(np.float64)
            extent_constricted = np.copy(voxel_bbox_extent).astype(np.float32)

            extent_dilated[:, 0] = extent_dilated[:, 0] - margin
            extent_dilated[:, 1] = extent_dilated[:, 1] + margin

            bbox_dilated = get_bounding_box_verteces(extent_dilated)
            (
                pc_dilated,
                axis_aligned_o3d_dilated,
                center_dilated,
                wlh_dilated,
                wlh_half_dilated,
                mesh_dilated,
                vertices_dilated,
            ) = self.transform_voxelized_bbox_to_mesh(bbox_dilated)

            extent_constricted[:, 0] = extent_constricted[:, 0] + margin
            extent_constricted[:, 1] = extent_constricted[:, 1] - margin

            if np.any(np.subtract(extent_constricted[:, 1], extent_constricted[:, 0]) < 1):
                too_small = np.subtract(extent_constricted[:, 1], extent_constricted[:, 0]) < 1
                extent_constricted[too_small, :] = voxel_bbox_extent[too_small, :]
                # extent_constricted = voxel_bbox_extent

            bbox_constricted = get_bounding_box_verteces(extent_constricted)
            (
                pc_constricted,
                axis_aligned_o3d_constricted,
                center_constricted,
                wlh_constricted,
                wlh_half_constricted,
                mesh_constricted,
                vertices_constricted,
            ) = self.transform_voxelized_bbox_to_mesh(bbox_constricted)

            axis_aligned_o3d_dilated.color = [1, 0, 0]
            axis_aligned_o3d_constricted.color = [0, 0, 1]

            if bVisualize:
                bbox_list = [self.axis_aligned_o3d, axis_aligned_o3d_dilated, axis_aligned_o3d_constricted]
                scene_name = "scene"
                scene_bbox_visualization(
                    scene_name,
                    self.mesh_model.mesh,
                    bbox_list,
                    bbox_line_width_px=5,
                    bbox_color=[0, 0, 0],
                    show_axes=False,
                )

                # o3d.visualization.draw_geometries(
                #     [
                #         self.mesh_model.mesh,
                #         self.axis_aligned_o3d,
                #         axis_aligned_o3d_dilated,
                #         axis_aligned_o3d_constricted,
                #     ],
                #     point_show_normal=True,
                # )
                stop = 1

            histogram_min_range = np.zeros([3, 2])
            histogram_min_range[0, :] = [min(vertices_dilated[:, 0]), min(vertices_constricted[:, 0])]  # x_min_range
            histogram_min_range[1, :] = [min(vertices_dilated[:, 1]), min(vertices_constricted[:, 1])]  # y_min_range
            histogram_min_range[2, :] = [min(vertices_dilated[:, 2]), min(vertices_constricted[:, 2])]  # z_min_range

            histogram_max_range = np.zeros([3, 2])
            histogram_max_range[0, :] = [max(vertices_constricted[:, 0]), max(vertices_dilated[:, 0])]
            histogram_max_range[1, :] = [max(vertices_constricted[:, 1]), max(vertices_dilated[:, 1])]
            histogram_max_range[2, :] = [max(vertices_constricted[:, 2]), max(vertices_dilated[:, 2])]

            return histogram_min_range, histogram_max_range

        def dilate_to_include_furniture_wall(
            self, extend_bbox_percentage, histogram_bins_width_metric, histogram_avg_threshold, bVisualize=False
        ):
            x, y, z = self.get_histogram_of_neighbouring_model_points(
                extend_bbox_percentage, histogram_bins_width_metric
            )
            self.get_dilated_extent(x, y, z, histogram_avg_threshold)
            self.vertices = get_bounding_box_verteces(self.extent)
            self.height = self.extent[1, 0]
            self.update_bbox()

            if bVisualize:
                o3d.visualization.draw_geometries(
                    [self.mesh_model.mesh, self.axis_aligned_o3d], point_show_normal=True,
                )
                stop = 1

        def dilate_to_include_furniture_wall_v2(
            self,
            histogram_min_range,
            histogram_max_range,
            histogram_bins_width_metric,
            histogram_avg_threshold,
            bVisualize=False,
        ):
            """
            Based on histogram range from voxelized model
            """
            vertices_histograms = self.get_histogram_of_neighbouring_model_points_v3(
                histogram_min_range, histogram_max_range, histogram_bins_width_metric
            )
            self.get_dilated_extent_v3(vertices_histograms, histogram_avg_threshold)
            self.vertices = get_bounding_box_verteces(self.extent)
            self.height = self.extent[1, 0]
            self.update_bbox()

            if bVisualize:
                o3d.visualization.draw_geometries(
                    [self.mesh_model.mesh, self.axis_aligned_o3d], point_show_normal=True,
                )
                stop = 1

        def get_histogram_of_neighbouring_model_points(self, limit, bins_width_metric):
            """ Extends bounding box vertices by +/- limit(%) in every direction (all axis). 
            Finds histogram of mesh model points in range of extendend bounding box points.  
            """
            bbox_min = np.amin(self.vertices, axis=0)
            bbox_max = np.amax(self.vertices, axis=0)

            # np.where(bbox_min > 0, bbox_min * (1 - limit), bbox_min * (1 + limit))
            # np.where(bbox_max > 0, bbox_max * (1 + limit), bbox_max * (1 - limit))

            bbox_min = np.where(bbox_min > 0, bbox_min * (1 - limit), bbox_min * (1 + limit))
            bbox_max = np.where(bbox_max > 0, bbox_max * (1 + limit), bbox_max * (1 - limit))

            extended_bbox_vertices_interval = np.logical_and(
                self.mesh_model.vertices[:, 0] >= bbox_min[0], self.mesh_model.vertices[:, 0] <= bbox_max[0]
            )
            mesh_model_part = self.mesh_model.vertices[extended_bbox_vertices_interval, :]

            extended_bbox_vertices_interval = np.logical_and(
                mesh_model_part[:, 1] >= bbox_min[1], mesh_model_part[:, 1] <= bbox_max[1]
            )
            mesh_model_part = mesh_model_part[extended_bbox_vertices_interval, :]

            extended_bbox_vertices_interval = np.logical_and(
                mesh_model_part[:, 2] >= bbox_min[2], mesh_model_part[:, 2] <= bbox_max[2]
            )
            mesh_model_part = mesh_model_part[extended_bbox_vertices_interval, :]

            for i in range(3):
                no_bins = int(np.ceil((bbox_max[i] - bbox_min[i]) / bins_width_metric))
                if i == 0:
                    vertices_histogram_x = np.histogram(
                        mesh_model_part[:, i], bins=no_bins, range=(bbox_min[0], bbox_max[0])
                    )
                    # plt.bar(
                    #     vertices_histogram_x[1][:-1],
                    #     vertices_histogram_x[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                elif i == 1:
                    vertices_histogram_y = np.histogram(
                        mesh_model_part[:, i], bins=no_bins, range=(bbox_min[1], bbox_max[1])
                    )
                    # plt.bar(
                    #     vertices_histogram_y[1][:-1],
                    #     vertices_histogram_y[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()

                else:
                    vertices_histogram_z = np.histogram(
                        mesh_model_part[:, i], bins=no_bins, range=(bbox_min[2], bbox_max[2])
                    )
                    # plt.bar(
                    #     vertices_histogram_z[1][:-1],
                    #     vertices_histogram_z[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()

            return vertices_histogram_x, vertices_histogram_y, vertices_histogram_z

        def get_histogram_of_neighbouring_model_points_v2(self, limit, bins_width_metric):
            """ Extends bounding box vertices by +/- limit(%) in each direction individually. 
            Finds histogram of mesh model points in range of extendend bounding box points in each dimension individualy 
            """

            bbox_min = np.amin(self.vertices, axis=0)
            bbox_max = np.amax(self.vertices, axis=0)

            bbox_min = np.where(bbox_min > 0, bbox_min * (1 - limit), bbox_min * (1 + limit))
            bbox_max = np.where(bbox_max > 0, bbox_max * (1 + limit), bbox_max * (1 - limit))

            for i in range(3):
                no_bins = int(np.ceil((bbox_max[i] - bbox_min[i]) / bins_width_metric))

                extended_bbox_vertices_interval = np.logical_and(
                    self.mesh_model.vertices[:, i] >= bbox_min[i], self.mesh_model.vertices[:, i] <= bbox_max[i]
                )
                mesh_model_part = self.mesh_model.vertices[extended_bbox_vertices_interval, :]
                if i == 0:
                    vertices_histogram_x = np.histogram(
                        mesh_model_part[:, i], bins=no_bins, range=(bbox_min[i], bbox_max[i])
                    )
                    # plt.bar(
                    #     vertices_histogram_x[1][:-1],
                    #     vertices_histogram_x[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                elif i == 1:
                    vertices_histogram_y = np.histogram(
                        mesh_model_part[:, i], bins=no_bins, range=(bbox_min[i], bbox_max[i])
                    )
                    # plt.bar(
                    #     vertices_histogram_y[1][:-1],
                    #     vertices_histogram_y[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()

                else:
                    vertices_histogram_z = np.histogram(
                        mesh_model_part[:, i], bins=no_bins, range=(bbox_min[i], bbox_max[i])
                    )
                    # plt.bar(
                    #     vertices_histogram_z[1][:-1],
                    #     vertices_histogram_z[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()

            return vertices_histogram_x, vertices_histogram_y, vertices_histogram_z

        def get_histogram_of_neighbouring_model_points_v3(
            self, histogram_range_min, histogram_range_max, bins_width_metric
        ):
            """ Extend voxelized bounding box by 1 voxel in each direction. Transforms extended voxelized bbox to "mesh" bbox. 
            Finds histogram of mesh model points in range of extendend mesh bounding box points in each dimension individualy 
            """

            ## novo
            extended_bbox_vertices_interval = np.logical_and(
                self.mesh_model.vertices[:, 0] >= histogram_range_min[0, 0],
                self.mesh_model.vertices[:, 0] <= histogram_range_max[0, 1],
            )
            mesh_model_part = self.mesh_model.vertices[extended_bbox_vertices_interval, :]

            extended_bbox_vertices_interval = np.logical_and(
                mesh_model_part[:, 1] >= histogram_range_min[1, 0], mesh_model_part[:, 1] <= histogram_range_max[1, 1]
            )
            mesh_model_part = mesh_model_part[extended_bbox_vertices_interval, :]

            extended_bbox_vertices_interval = np.logical_and(
                mesh_model_part[:, 2] >= histogram_range_min[2, 0], mesh_model_part[:, 2] <= histogram_range_max[2, 1]
            )
            mesh_model_part = mesh_model_part[extended_bbox_vertices_interval, :]
            ##

            vertices_histograms = []
            for i in range(3):
                no_bins_min = int(
                    np.ceil((abs(histogram_range_min[i, 1] - histogram_range_min[i, 0])) / bins_width_metric)
                )
                no_bins_max = int(
                    np.ceil((abs(histogram_range_max[i, 1] - histogram_range_max[i, 0])) / bins_width_metric)
                )

                bbox_min_range_vertices = np.logical_and(
                    self.mesh_model.vertices[:, i] >= histogram_range_min[i, 0],
                    self.mesh_model.vertices[:, i] <= histogram_range_min[i, 1],
                )

                bbox_max_range_vertices = np.logical_and(
                    self.mesh_model.vertices[:, i] >= histogram_range_max[i, 0],
                    self.mesh_model.vertices[:, i] <= histogram_range_max[i, 1],
                )

                mesh_model_part_min = self.mesh_model.vertices[bbox_min_range_vertices, :]
                mesh_model_part_max = self.mesh_model.vertices[bbox_max_range_vertices, :]
                if i == 0:
                    # vertices_histogram_x_min = np.histogram(
                    #     mesh_model_part_min[:, i],
                    #     bins=no_bins_min,
                    #     range=(histogram_range_min[i, 0], histogram_range_min[i, 1]),
                    # )
                    # vertices_histogram_x_max = np.histogram(
                    #     mesh_model_part_max[:, i],
                    #     bins=no_bins_max,
                    #     range=(histogram_range_max[i, 0], histogram_range_max[i, 1]),
                    # )

                    ##novo
                    vertices_histogram_x_min = np.histogram(
                        mesh_model_part[:, i],
                        bins=no_bins_min,
                        range=(histogram_range_min[i, 0], histogram_range_min[i, 1]),
                    )
                    vertices_histogram_x_max = np.histogram(
                        mesh_model_part[:, i],
                        bins=no_bins_max,
                        range=(histogram_range_max[i, 0], histogram_range_max[i, 1]),
                    )
                    ##

                    vertices_histograms.append(vertices_histogram_x_min)
                    vertices_histograms.append(vertices_histogram_x_max)
                    # plt.bar(
                    #     vertices_histogram_x_min[1][:-1],
                    #     vertices_histogram_x_min[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                    # plt.bar(
                    #     vertices_histogram_x_max[1][:-1],
                    #     vertices_histogram_x_max[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                elif i == 1:
                    # vertices_histogram_y_min = np.histogram(
                    #     mesh_model_part_min[:, i],
                    #     bins=no_bins_min,
                    #     range=(histogram_range_min[i, 0], histogram_range_min[i, 1]),
                    # )
                    # vertices_histogram_y_max = np.histogram(
                    #     mesh_model_part_max[:, i],
                    #     bins=no_bins_max,
                    #     range=(histogram_range_max[i, 0], histogram_range_max[i, 1]),
                    # )

                    ## novo
                    vertices_histogram_y_min = np.histogram(
                        mesh_model_part[:, i],
                        bins=no_bins_min,
                        range=(histogram_range_min[i, 0], histogram_range_min[i, 1]),
                    )
                    vertices_histogram_y_max = np.histogram(
                        mesh_model_part[:, i],
                        bins=no_bins_max,
                        range=(histogram_range_max[i, 0], histogram_range_max[i, 1]),
                    )
                    ##
                    vertices_histograms.append(vertices_histogram_y_min)
                    vertices_histograms.append(vertices_histogram_y_max)
                    # plt.bar(
                    #     vertices_histogram_y_min[1][:-1],
                    #     vertices_histogram_y_min[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                    # plt.bar(
                    #     vertices_histogram_y_max[1][:-1],
                    #     vertices_histogram_y_max[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                else:
                    # vertices_histogram_z_min = np.histogram(
                    #     mesh_model_part_min[:, i],
                    #     bins=no_bins_min,
                    #     range=(histogram_range_min[i, 0], histogram_range_min[i, 1]),
                    # )
                    # vertices_histogram_z_max = np.histogram(
                    #     mesh_model_part_max[:, i],
                    #     bins=no_bins_max,
                    #     range=(histogram_range_max[i, 0], histogram_range_max[i, 1]),
                    # )

                    # novo
                    vertices_histogram_z_min = np.histogram(
                        mesh_model_part[:, i],
                        bins=no_bins_min,
                        range=(histogram_range_min[i, 0], histogram_range_min[i, 1]),
                    )
                    vertices_histogram_z_max = np.histogram(
                        mesh_model_part[:, i],
                        bins=no_bins_max,
                        range=(histogram_range_max[i, 0], histogram_range_max[i, 1]),
                    )
                    #

                    vertices_histograms.append(vertices_histogram_z_min)
                    vertices_histograms.append(vertices_histogram_z_max)
                    # plt.bar(
                    #     vertices_histogram_z_min[1][:-1],
                    #     vertices_histogram_z_min[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                    # plt.bar(
                    #     vertices_histogram_z_max[1][:-1],
                    #     vertices_histogram_z_max[0],
                    #     width=bins_width_metric,
                    #     align="edge",
                    #     edgecolor="black",
                    # )
                    # plt.show()
                    stop = 1

            stop = 1
            return vertices_histograms

        def get_dilated_extent(
            self, vertices_histogram_x, vertices_histogram_y, vertices_histogram_z, histogram_avg_threshold=3
        ):

            hist_x_average = np.average(vertices_histogram_x[0])
            hist_y_average = np.average(vertices_histogram_y[0])
            hist_z_average = np.average(vertices_histogram_z[0])

            width_mid_id = self.middle_element_id(vertices_histogram_x[1])
            height_mid_id = self.middle_element_id(vertices_histogram_y[1])
            lenght_mid_id = self.middle_element_id(vertices_histogram_z[1])

            # x_min
            if (~(vertices_histogram_x[0][width_mid_id::-1] > (histogram_avg_threshold * hist_x_average))).all():
                if (vertices_histogram_x[0][0:width_mid_id:] == 0).all():
                    self.extent[0, 0] = self.vertices[:, 0].min()
                elif (vertices_histogram_x[0][0:width_mid_id] != 0).all():
                    self.extent[0, 0] = vertices_histogram_x[1][0]

                else:
                    # if shelf does not have side panel, find beginning of the shelf
                    id = np.argmax(vertices_histogram_x[0][0:width_mid_id] > 0)
                    self.extent[0, 0] = vertices_histogram_x[1][id + 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_x[0][width_mid_id::-1] > (histogram_avg_threshold * hist_x_average)
                )
                self.extent[0, 0] = vertices_histogram_x[1][width_mid_id - id_max]

            # x_max
            if (~(vertices_histogram_x[0][width_mid_id:] > (histogram_avg_threshold * hist_x_average))).all():
                if (vertices_histogram_x[0][width_mid_id:] == 0).all():
                    self.extent[0, 1] = self.vertices[:, 0].max()
                elif (vertices_histogram_x[0][width_mid_id:] != 0).all():
                    self.extent[0, 1] = vertices_histogram_x[1][-1]

                else:
                    # if shelf does not have side panel, find shelf end
                    id = np.argmax(vertices_histogram_x[0][width_mid_id:] == 0)
                    x_max = width_mid_id + id - 1
                    self.extent[0, 1] = vertices_histogram_x[1][width_mid_id + id - 1]
            else:
                id_max = np.argmax(vertices_histogram_x[0][width_mid_id:] > (histogram_avg_threshold * hist_x_average))
                self.extent[0, 1] = vertices_histogram_x[1][width_mid_id + id_max + 1]

            # y_min
            if (~(vertices_histogram_y[0][height_mid_id::-1] > (histogram_avg_threshold * hist_y_average))).all():
                if (vertices_histogram_y[0][0:height_mid_id] == 0).all():
                    self.extent[1, 0] = self.vertices[:, 1].min()
                elif (vertices_histogram_y[0][0:height_mid_id] != 0).all():
                    self.extent[1, 0] = vertices_histogram_y[1][0]

                else:
                    id = np.argmax(vertices_histogram_y[0][0:height_mid_id] > 0)
                    self.extent[1, 0] = vertices_histogram_y[1][id + 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_y[0][height_mid_id::-1] > (histogram_avg_threshold * hist_y_average)
                )
                self.extent[1, 0] = vertices_histogram_y[1][height_mid_id - id_max]

            # y_max
            if (~(vertices_histogram_y[0][height_mid_id:] > (histogram_avg_threshold * hist_y_average))).all():
                if (vertices_histogram_y[0][height_mid_id:] == 0).all():  # this is the top shelf
                    self.extent[1, 1] = self.vertices[:, 1].max()
                    # model_height = np.amax(self.mesh_model.vertices[:, 1]) - np.amin(self.mesh_model.vertices[:, 1])
                    # self.extent[1, 1] = self.vertices[:, 1].min() + model_height / 2
                elif (vertices_histogram_y[0][height_mid_id:] != 0).all():
                    self.extent[1, 1] = vertices_histogram_y[1][-1]

                else:
                    id = np.argmax(vertices_histogram_y[0][height_mid_id:] == 0)
                    self.extent[1, 1] = vertices_histogram_y[1][height_mid_id + id - 1]
            else:
                id_max = np.argmax(vertices_histogram_y[0][height_mid_id:] > (histogram_avg_threshold * hist_y_average))
                self.extent[1, 1] = vertices_histogram_y[1][height_mid_id + id_max + 1]

            # z_min
            if (~(vertices_histogram_z[0][lenght_mid_id::-1] > (histogram_avg_threshold * hist_z_average))).all():
                if (vertices_histogram_z[0][0:lenght_mid_id] == 0).all():
                    self.extent[2, 0] = self.vertices[:, 2].min()
                elif (vertices_histogram_z[0][0:lenght_mid_id] != 0).all():
                    self.extent[2, 0] = vertices_histogram_z[1][0]

                else:
                    id = np.argmax(vertices_histogram_z[0][0:lenght_mid_id] > 0)
                    self.extent[2, 0] = vertices_histogram_z[1][id + 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_z[0][lenght_mid_id::-1] > (histogram_avg_threshold * hist_z_average)
                )
                self.extent[2, 0] = vertices_histogram_z[1][lenght_mid_id - id_max]

            # z_max
            if (~(vertices_histogram_z[0][lenght_mid_id:] > (histogram_avg_threshold * hist_z_average))).all():
                if (vertices_histogram_z[0][lenght_mid_id:] == 0).all():
                    self.extent[2, 1] = self.vertices[:, 2].max()
                elif (vertices_histogram_z[0][lenght_mid_id:] != 0).all():
                    id = np.argmax(vertices_histogram_z[0][lenght_mid_id:])
                    self.extent[2, 1] = vertices_histogram_z[1][-1]

                else:
                    id = np.argmax(vertices_histogram_z[0][lenght_mid_id:] == 0)
                    self.extent[2, 1] = vertices_histogram_z[1][lenght_mid_id + id - 1]
            else:
                id_max = np.argmax(vertices_histogram_z[0][lenght_mid_id:] > (histogram_avg_threshold * hist_z_average))
                self.extent[2, 1] = vertices_histogram_z[1][lenght_mid_id + id_max + 1]

        def get_dilated_extent_v2(
            self, vertices_histogram_x, vertices_histogram_y, vertices_histogram_z, histogram_avg_threshold=3
        ):
            """
                Max coordinates are picked from histograms from the end of the list to middle element
            """

            hist_x_average = np.average(vertices_histogram_x[0])
            hist_y_average = np.average(vertices_histogram_y[0])
            hist_z_average = np.average(vertices_histogram_z[0])

            width_mid_id = self.middle_element_id(vertices_histogram_x[1])
            height_mid_id = self.middle_element_id(vertices_histogram_y[1])
            lenght_mid_id = self.middle_element_id(vertices_histogram_z[1])

            # x_min
            if (~(vertices_histogram_x[0][0:width_mid_id] > (histogram_avg_threshold * hist_x_average))).all():
                if (vertices_histogram_x[0][0:width_mid_id:] == 0).all():
                    self.extent[0, 0] = self.vertices[:, 0].min()
                elif (vertices_histogram_x[0][0:width_mid_id] != 0).all():
                    self.extent[0, 0] = vertices_histogram_x[1][0]

                else:
                    # if shelf does not have side panel, find beginning of the shelf
                    id = np.argmax(vertices_histogram_x[0][0:width_mid_id] > 0)
                    self.extent[0, 0] = vertices_histogram_x[1][id + 1]
            else:
                id_max = np.argmax(vertices_histogram_x[0][0:width_mid_id] > (histogram_avg_threshold * hist_x_average))
                self.extent[0, 0] = vertices_histogram_x[1][id_max + 1]

            # x_max
            if (~(vertices_histogram_x[0][width_mid_id:] > (histogram_avg_threshold * hist_x_average))).all():
                if (vertices_histogram_x[0][width_mid_id:] == 0).all():
                    self.extent[0, 1] = self.vertices[:, 0].max()
                elif (vertices_histogram_x[0][width_mid_id:] != 0).all():
                    self.extent[0, 1] = vertices_histogram_x[1][-1]

                else:
                    # if shelf does not have side panel, find shelf end
                    id = np.argmax(vertices_histogram_x[0][width_mid_id:] == 0)
                    x_max = width_mid_id + id - 1
                    self.extent[0, 1] = vertices_histogram_x[1][width_mid_id + id - 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_x[0][: (width_mid_id - 1) : -1] > (histogram_avg_threshold * hist_x_average)
                )
                self.extent[0, 1] = vertices_histogram_x[1][-id_max - 1]

            # y_min
            if (~(vertices_histogram_y[0][0:height_mid_id] > (histogram_avg_threshold * hist_y_average))).all():
                if (vertices_histogram_y[0][0:height_mid_id] == 0).all():
                    self.extent[1, 0] = self.vertices[:, 1].min()
                elif (vertices_histogram_y[0][0:height_mid_id] != 0).all():
                    self.extent[1, 0] = vertices_histogram_y[1][0]

                else:
                    id = np.argmax(vertices_histogram_y[0][0:height_mid_id] > 0)
                    self.extent[1, 0] = vertices_histogram_y[1][id + 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_y[0][0:height_mid_id] > (histogram_avg_threshold * hist_y_average)
                )
                self.extent[1, 0] = vertices_histogram_y[1][id_max + 1]

            # y_max
            if (~(vertices_histogram_y[0][height_mid_id:] > (histogram_avg_threshold * hist_y_average))).all():
                if (vertices_histogram_y[0][height_mid_id:] == 0).all():  # this is the top shelf
                    self.extent[1, 1] = self.vertices[:, 1].max()
                    # model_height = np.amax(self.mesh_model.vertices[:, 1]) - np.amin(self.mesh_model.vertices[:, 1])
                    # self.extent[1, 1] = self.vertices[:, 1].min() + model_height / 2
                elif (vertices_histogram_y[0][height_mid_id:] != 0).all():
                    self.extent[1, 1] = vertices_histogram_y[1][-1]

                else:
                    id = np.argmax(vertices_histogram_y[0][height_mid_id:] == 0)
                    self.extent[1, 1] = vertices_histogram_y[1][height_mid_id + id - 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_y[0][: (height_mid_id - 1) : -1] > (histogram_avg_threshold * hist_y_average)
                )
                self.extent[1, 1] = vertices_histogram_y[1][-id_max - 1]

            # z_min
            if (~(vertices_histogram_z[0][0:lenght_mid_id] > (histogram_avg_threshold * hist_z_average))).all():
                if (vertices_histogram_z[0][0:lenght_mid_id] == 0).all():
                    self.extent[2, 0] = self.vertices[:, 2].min()
                elif (vertices_histogram_z[0][0:lenght_mid_id] != 0).all():
                    self.extent[2, 0] = vertices_histogram_z[1][0]

                else:
                    id = np.argmax(vertices_histogram_z[0][0:lenght_mid_id] > 0)
                    self.extent[2, 0] = vertices_histogram_z[1][id + 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_z[0][0:lenght_mid_id] > (histogram_avg_threshold * hist_z_average)
                )
                self.extent[2, 0] = vertices_histogram_z[1][id_max + 1]

            # z_max
            if (~(vertices_histogram_z[0][lenght_mid_id:] > (histogram_avg_threshold * hist_z_average))).all():
                if (vertices_histogram_z[0][lenght_mid_id:] == 0).all():
                    self.extent[2, 1] = self.vertices[:, 2].max()
                elif (vertices_histogram_z[0][lenght_mid_id:] != 0).all():
                    id = np.argmax(vertices_histogram_z[0][lenght_mid_id:])
                    self.extent[2, 1] = vertices_histogram_z[1][-1]

                else:
                    id = np.argmax(vertices_histogram_z[0][lenght_mid_id:] == 0)
                    self.extent[2, 1] = vertices_histogram_z[1][lenght_mid_id + id - 1]
            else:
                id_max = np.argmax(
                    vertices_histogram_z[0][: (lenght_mid_id - 1) : -1] > (histogram_avg_threshold * hist_z_average)
                )
                self.extent[2, 1] = vertices_histogram_z[1][-id_max - 1]

        def get_dilated_extent_v3(self, vertices_histograms, histogram_avg_threshold=3):
            """
                Based on voxelized histograms
                vertices_histograms: x_min, x_max, y_min, y_max, z_min, z_max
            """

            for i, iAxis_min, iAxis_max in zip(range(3), range(0, 7, 2), range(1, 7, 2)):
                histogram_min = vertices_histograms[iAxis_min]
                histogram_max = vertices_histograms[iAxis_max]

                hist_average_min = np.average(histogram_min[0])
                hist_average_max = np.average(histogram_max[0])

                min_mid_id = self.middle_element_id(histogram_min[1])
                max_mid_id = self.middle_element_id(histogram_max[1])

                # min
                if (histogram_min[0] > (histogram_avg_threshold * hist_average_min)).any():
                    id_max = np.argmax(histogram_min[0][::-1] > (histogram_avg_threshold * hist_average_min))
                    self.extent[i, 0] = histogram_min[1][-id_max - 2]
                    # self.extent[i, 0] = histogram_min[1][-id_max]
                    stop = 1
                else:
                    if (histogram_min[0] == 0).all():
                        self.extent[i, 0] = self.vertices[:, i].min()
                    else:
                        id_max = np.argmax(histogram_min[0])
                        self.extent[i, 0] = histogram_min[1][id_max]

                # max
                if (histogram_max[0] > (histogram_avg_threshold * hist_average_max)).any():
                    id_max = np.argmax(histogram_max[0] > (histogram_avg_threshold * hist_average_max))
                    self.extent[i, 1] = histogram_max[1][id_max + 1]
                else:
                    if (histogram_max[0] == 0).all():
                        self.extent[i, 1] = self.vertices[:, i].max()
                    else:
                        id_max = np.argmax(histogram_max[0])
                        self.extent[i, 1] = histogram_max[1][id_max + 1]

        def middle_element_id(self, lst):
            half_len = int((len(lst) / 2))
            if len(lst) % 2 == 0:
                return half_len - 1
            else:
                return half_len

