import os
import sys
import logging
from zlib import Z_DEFAULT_COMPRESSION
import numpy as np
from numpy.lib.arraysetops import ediff1d, unique
import scipy as sc

import cc3d
import open3d as o3d
from skimage.feature import peak_local_max

from matplotlib import pyplot as plt
import tkinter
from mpl_toolkits.mplot3d import Axes3D
from torch import int8

from itertools import product, combinations

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.dirname(BASE_DIR)
sys.path.append(BASE_DIR)
sys.path.append(os.path.join(ROOT_DIR, "Third_party"))

from binvox_rw import (
    read_as_3d_array,
    Voxels,
)  # Python module to read and write .binvox files, downloaded from:  https://github.com/dimatura/binvox-rw-py


def open3d_vis(model_voxels_grid, cores_voxels_grid):
    ## NOVO
    color = [1.0, 0.7, 0.0]
    cubic_size = 1.0
    voxel_resolution = 32.0
    voxel_size = cubic_size / voxel_resolution
    origin = [-cubic_size / 2.0, -cubic_size / 2.0, -cubic_size / 2.0]

    x, y, z = np.where(cores_voxels_grid == 1)
    index_voxel = np.vstack((x, y, z))
    grid_index_array = index_voxel.T

    pointcloud_array = grid_index_array * voxel_size
    pc = o3d.geometry.PointCloud()
    pc.points = o3d.utility.Vector3dVector(pointcloud_array)
    pc.paint_uniform_color([1.0, 0.7, 0.0])
    o3d_voxel = o3d.geometry.VoxelGrid.create_from_point_cloud(pc, voxel_size=voxel_size)

    # line_color = [0.25, 0.25, 0.5]
    # colors = [line_color for i in o3d_voxel.LineSet]
    # o3d_voxel.LineSet.colors = o3d.utility.Vector3dVector(colors)

    # lines = o3d.geometry.LineSet
    # lines.LineSet = o3d_voxel.LineSet
    # lines.colors = [0.25, 0.25, 0.5]

    x, y, z = np.where(model_voxels_grid == 1)
    index_voxel = np.vstack((x, y, z))
    grid_index_array = index_voxel.T

    pointcloud_array_model = grid_index_array * voxel_size
    pc_model = o3d.geometry.PointCloud()
    pc_model.points = o3d.utility.Vector3dVector(pointcloud_array_model)
    pc_model.paint_uniform_color([0.5, 0.5, 1])
    o3d_voxel_model = o3d.geometry.VoxelGrid.create_from_point_cloud(pc_model, voxel_size=voxel_size)
    # o3d_voxel_model.LineSet.paint_uniform_color([0.5, 0.35, 0.05])

    o3d.visualization.draw_geometries([o3d_voxel, o3d_voxel_model])
    stop = 1

    ##


def get_bounding_box_verteces(extent):

    """ Finds all vertices of the voxelized bounding box. 

    Parameters
    ---------
    bounding_box_coords : ndarray, shape(3, 2) 
        cooridnates (xyz) are written in rows
        first column contains minimal coordinates, second column contains maximal coordinates

    Returns
    -------
    self.vertices : ndarray, shape (8,3)
        coordinates of the voxelized bounding box vertices
    """

    vertices = np.zeros((8, 3))

    iPoint = 0
    for y in range(2):
        for x in range(2):
            for z in range(2):
                vertices[iPoint, 0] = extent[0, x]
                vertices[iPoint, 1] = extent[1, y]
                vertices[iPoint, 2] = extent[2, z]
                iPoint += 1

    return vertices


def cuboid_data(center, size):
    """
    Create a data array for cuboid plotting.


    ============= ================================================
    Argument      Description
    ============= ================================================
    center        center of the cuboid, triple
    size          size of the cuboid, triple, (x_length,y_width,z_height)
    :type size: tuple, numpy.array, list
    :param size: size of the cuboid, triple, (x_length,y_width,z_height)
    :type center: tuple, numpy.array, list
    :param center: center of the cuboid, triple, (x,y,z)


    """

    # suppose axis direction: x: to left; y: to inside; z: to upper
    # get the (left, outside, bottom) point
    o = [a - b / 2 for a, b in zip(center, size)]
    # get the length, width, and height
    l, w, h = size
    x = [
        [o[0], o[0] + l, o[0] + l, o[0], o[0]],  # x coordinate of points in bottom surface
        [o[0], o[0] + l, o[0] + l, o[0], o[0]],  # x coordinate of points in upper surface
        [o[0], o[0] + l, o[0] + l, o[0], o[0]],  # x coordinate of points in outside surface
        [o[0], o[0] + l, o[0] + l, o[0], o[0]],
    ]  # x coordinate of points in inside surface
    y = [
        [o[1], o[1], o[1] + w, o[1] + w, o[1]],  # y coordinate of points in bottom surface
        [o[1], o[1], o[1] + w, o[1] + w, o[1]],  # y coordinate of points in upper surface
        [o[1], o[1], o[1], o[1], o[1]],  # y coordinate of points in outside surface
        [o[1] + w, o[1] + w, o[1] + w, o[1] + w, o[1] + w],
    ]  # y coordinate of points in inside surface
    z = [
        [o[2], o[2], o[2], o[2], o[2]],  # z coordinate of points in bottom surface
        [o[2] + h, o[2] + h, o[2] + h, o[2] + h, o[2] + h],  # z coordinate of points in upper surface
        [o[2], o[2], o[2] + h, o[2] + h, o[2]],  # z coordinate of points in outside surface
        [o[2], o[2], o[2] + h, o[2] + h, o[2]],
    ]  # z coordinate of points in inside surface
    return x, y, z


class BinvoxModel(Voxels):
    def __init__(self, data, dims, translate, scale, axis_order):
        super().__init__(data, dims, translate, scale, axis_order)
        # self.binvox = self.read_binvox_file(file_path)

        self.voxels = self.data.astype(np.float32)
        self.voxels_inverted = 1 - self.voxels
        self.size = len(self.voxels)

        self.mesh = {}

        self.occupancy_distances = {}
        self.distances_local_max = {}
        self.free_space_cores = {}

        self.storage_volumes = []

    @staticmethod
    def read_binvox_file(file_path):
        """Loads binvox file (voxelized CAD model) in a 3D array

        Parameters
        ---------
        file_path : Absolute file path to binvox file

        Returns
        -------
        Voxels object (data - 3d boolean array, 
                       dims - 2d array, 
                       translate - translation vector used for creating voxelized model from mesh (in Shapenet), 
                       scale - float, scale used for creating voxelized model from mesh (in Shapenet), 
                       axis_order - string)
        """

        try:
            with open(file_path, "rb") as f:
                return read_as_3d_array(f, True, BinvoxModel)

        except FileNotFoundError:
            logging.error(f"Binvox file {file_path} not found")
            raise Exception("Error happened. Could not find binvox file.")

    def detect_storage_volumes(self, bounding_box_size_threshold, flat_surface_threshold):
        print("\tDetecting storage volumes in voxelized model.\n")
        self.detect_free_space_cores()
        self.get_storage_volume_bounding_boxes(bounding_box_size_threshold, flat_surface_threshold)

        # TODO: napraviti bolju funkciju za remove noise (trenutno uzima prvi box koji se preklapa, a mozda bi trebao najveci?)
        self.remove_noise_bboxes()

    def dilate_model_size_bbox(self):
        dilate_size = 3
        grid_size = self.size + dilate_size * 2
        voxels_dilated = np.zeros((grid_size, grid_size, grid_size))
        voxels_dilated[
            dilate_size : grid_size - dilate_size,
            dilate_size : grid_size - dilate_size,
            dilate_size : grid_size - dilate_size,
        ] = self.voxels
        self.voxels = voxels_dilated
        self.voxels_inverted = 1 - self.voxels
        self.size = grid_size

        # self.visualize_voxels_model_and_ROI(self.voxels)

    def detect_free_space_cores(self, connectivity=6, bVisualize=False):
        self.occupancy_distances = self.get_3D_distance_transform()
        self.distances_local_max = self.get_distances_local_maxima()
        self.free_space_cores = self.get_connected_components(self.distances_local_max, connectivity)

        if bVisualize:
            self.visualize_voxels_model_and_ROI(self.distances_local_max)
            stop = 1

    def get_3D_distance_transform(self):
        """ Calculates 3D distance transform of inverted voxels grid (3D array)
            
            This function calculates the distance transform of the input, by replacing each foreground (non-zero) element, 
            with its shortest distance to the background (any zero-valued element).

        Returns
        -------
        3D array (int)
        """

        return sc.ndimage.distance_transform_bf(
            self.voxels_inverted,
            metric="chessboard",
            sampling=None,
            return_distances=True,
            return_indices=False,
            distances=None,
            indices=None,
        )

    def get_distances_local_maxima(self, order=1):
        """Detects local maxima in occupancy distances grid (3D array).

        Parameters
        ---------
        order : int
            How many cells on each side to use for the comparison

        Returns
        -------
        coords : ndarray
            coordinates of the local maxima

        """
        size = 1 + 2 * order
        footprint = np.ones((size, size, size))
        footprint[order, order, order] = 0

        filtered = sc.ndimage.maximum_filter(self.occupancy_distances, footprint=footprint)
        mask_local_maxima = np.logical_and(self.occupancy_distances >= filtered, self.voxels == 0)

        local_max_coords = np.asarray(np.where(mask_local_maxima)).T

        # voxels_local_max_grid = np.zeros((self.size, self.size, self.size)).astype(np.int32)
        # for i in range(len(local_max_coords)):
        #     xyz = tuple(local_max_coords[i])
        #     voxels_local_max_grid[xyz] = 1

        voxels_local_max_grid = self.get_voxels_grid(local_max_coords)

        return voxels_local_max_grid

    def get_voxels_grid(self, ROI_voxels):
        voxels_grid_annoted = np.zeros((self.size, self.size, self.size)).astype(np.int32)

        for iVoxel in range(len(ROI_voxels)):
            xyz = tuple(ROI_voxels[iVoxel])
            voxels_grid_annoted[xyz] = 1

        return voxels_grid_annoted

    def get_voxels_grid_from_2D_ROI(self, ROI_2D, height):
        voxels_grid = np.zeros([self.size, self.size, self.size])

        for voxel in ROI_2D:
            j, l = tuple(voxel)
            j = int(j)
            k = int(height)
            l = int(l)
            voxels_grid[j, k, l] = 1

        return voxels_grid

    def get_connected_components(self, distances_local_max, connectivity):
        """Group and label local maxima occupancy distances in occupancy distances grid (3D array)

        Parameters
        ---------
        connectivity: int
            only 4,8 (2D) and 26, 18, and 6 (3D) are allowed
        
        Returns
        -------
        ndarray
            array filled with connected component labels

        """
        # https://pypi.org/project/connected-components-3d/
        return cc3d.connected_components(distances_local_max, connectivity=connectivity)

    def remove_noise_bboxes(self, bVisualize=False):
        # TODO: poredati bboxove od dolje prema gore, s ijeva na desno pa onda birati po tome hocemo li izbaciti prvi..zadnji...
        extents_all = [x.extent for x in self.storage_volumes]
        heights_all = [x.supporting_surface_height for x in self.storage_volumes]

        remove_bboxes = []

        for i, iHeight in enumerate(heights_all):
            height_neighborhood = np.logical_or(heights_all == iHeight - 1, heights_all == iHeight + 1)
            height_neighborhood = np.logical_or(height_neighborhood, heights_all == iHeight)

            for j, in_neighborhood in enumerate(height_neighborhood):
                if i == j:
                    continue

                if in_neighborhood:
                    if np.logical_and(
                        extents_all[j][0, 0] - 1 <= extents_all[i][0, 0],
                        extents_all[j][0, 1] + 1 >= extents_all[i][0, 1],
                    ):
                        if np.logical_and(
                            extents_all[j][2, 0] - 1 <= extents_all[i][2, 0],
                            extents_all[j][2, 1] + 1 >= extents_all[i][2, 1],
                        ):
                            remove_bboxes.append(i)

                    if np.logical_and(
                        extents_all[i][0, 0] - 1 <= extents_all[j][0, 0],
                        extents_all[i][0, 1] + 1 >= extents_all[j][0, 1],
                    ):
                        if np.logical_and(
                            extents_all[i][2, 0] - 1 <= extents_all[j][2, 0],
                            extents_all[i][2, 1] + 1 >= extents_all[j][2, 1],
                        ):
                            remove_bboxes.append(j)

        remove_bboxes = np.unique(remove_bboxes)

        deleted = 0
        for element in remove_bboxes:
            del self.storage_volumes[element - deleted]
            deleted += 1

        if bVisualize:
            for bbox in self.storage_volumes:
                bbox.visualize_bounding_box()

                surface_below_bbox_3D = np.zeros([self.size, self.size, self.size])
                surface_below_bbox_3D[:, bbox.extent[1, 0], :] = bbox.supporting_layer
                # self.visualize_voxels_model_and_ROI(surface_below_bbox_3D)
                stop = 1

    def get_storage_volume_bounding_boxes(
        self, bbox_size_threshold, supporting_surface_coverage_threshold, bVisualize_each=False, bVisualize_all=False
    ):

        # TODO: add flag if the storage volume is on the top of the furniture
## for debugging
        bVisualization = False
        iFreeSpaceCore_dbg = 6 
        iAxis_dbg = 1 
        iAxis_value_dbg = 8
## end debugging

        for iFreeSpaceCore in range(1, np.max(self.free_space_cores) + 1):
            free_space_core_cells = np.argwhere(self.free_space_cores == iFreeSpaceCore)

## for debugging
            if iFreeSpaceCore == iFreeSpaceCore_dbg:
                voxels_local_max_grid = self.get_voxels_grid(free_space_core_cells)
                
                if bVisualization:
                    self.visualize_voxels_model_and_ROI(voxels_local_max_grid)
            stop = 1
## end debugging

            storage_volumes = []
            for iAxis in range(3):
                axis_values = np.unique(free_space_core_cells[:, iAxis])

                volume0 = 0
                largest_bbox = []
                for iAxis_value in axis_values:

                    storage_volume_candidate = self._bbox(self)
                    storage_volume_candidate.free_space_core_slice = free_space_core_cells[
                        free_space_core_cells[:, iAxis] == iAxis_value
                    ]

## for debugging
                    if np.logical_and(np.logical_and(iFreeSpaceCore == iFreeSpaceCore_dbg, iAxis == iAxis_dbg), iAxis_value == iAxis_value_dbg):
                        free_space_core_slice_in_grid = self.get_voxels_grid(
                            storage_volume_candidate.free_space_core_slice
                        )

                        if bVisualization:
                            self.visualize_voxels_model_and_ROI(free_space_core_slice_in_grid)
## end debugging

                    free_space_core_slice_layer = np.copy(storage_volume_candidate.free_space_core_slice)
                    free_space_core_slice_layer = np.delete(free_space_core_slice_layer, iAxis, axis=1)

                    " Check if this slice is one component"
                    slice_layer = np.zeros([self.size, self.size], dtype=int)
                    for i in free_space_core_slice_layer:
                        slice_layer[tuple(i)] = 1

                    x = np.nonzero(slice_layer)  # has at least one non_zero
                    if x[0].size == 0:
                        stop = 1

                    if x[0].size > 1:
                        if x[0].size == 2:
                            stop = 1
                            # check if two voxels are connected, because cc3d does not work with one voxel
                            condition_i = np.logical_not(np.ediff1d(x[0]) == 0, abs(np.ediff1d(x[1])) == 1)
                            condition_ii = np.logical_not(np.ediff1d(x[0]) == 1, abs(np.ediff1d(x[1])) == 0)
                            condition = np.logical_and(condition_i, condition_ii)
                            if condition:
                                stop = 1
                                continue
                        else:
                            stop = 1
                            slice_layer = np.pad(slice_layer, pad_width=1, mode="constant", constant_values=0)
                            connectivity = 4
                            layer_components = self.get_connected_components(slice_layer, connectivity)
                            if np.sum(layer_components) == 0:
                                # self.visualize_voxels_model_and_ROI(free_space_core_slice_in_grid)
                                stop = 1

                            "layer has more than one component"
                            if layer_components.max() != 1:
                                # self.visualize_voxels_model_and_ROI(free_space_core_slice_in_grid)
                                stop = 1
                                continue

                    " Check if this slice is squared shaped"
                    j, j_counts = np.unique(free_space_core_slice_layer[:, 0], return_counts=True)
                    k, k_counts = np.unique(free_space_core_slice_layer[:, 1], return_counts=True)

                    if (j.size * k.size) > len(free_space_core_slice_layer):
                        stop = 1
                        # self.visualize_voxels_model_and_2D_ROI(free_space_core_slice_layer)

                        jAxis = iAxis - 1
                        kAxis = iAxis - 2

                        if np.logical_and(jAxis < 0, kAxis < 0):
                            tmp = jAxis
                            jAxis = kAxis
                            kAxis = tmp

                        slice_min_coords = tuple(np.amin(free_space_core_slice_layer, axis=0))
                        slice_max_coords = tuple(np.amax(free_space_core_slice_layer, axis=0))

                        free_space_core_slice_layer_adjusted = []
                        for j in range(slice_min_coords[0], slice_max_coords[0] + 1, 1):
                            for k in range(slice_min_coords[1], slice_max_coords[1] + 1, 1):
                                item = np.zeros([3], dtype=int)
                                item[iAxis] = iAxis_value
                                item[jAxis] = j
                                item[kAxis] = k
                                free_space_core_slice_layer_adjusted.append(item)

## for debugging
                        if np.logical_and(np.logical_and(iFreeSpaceCore == iFreeSpaceCore_dbg, iAxis == iAxis_dbg), iAxis_value == iAxis_value_dbg):
                            voxels_slice_grid = self.get_voxels_grid(free_space_core_slice_layer_adjusted)
                            
                            if bVisualization:
                                self.visualize_voxels_model_and_ROI(voxels_slice_grid)
                            stop = 1
## end debugging

                        storage_volume_candidate.free_space_core_slice = np.asarray(
                            free_space_core_slice_layer_adjusted
                        )
                        stop = 1

                    if storage_volume_candidate.free_space_core_slice.size == 0:
                        stop = 1
                        continue

                    storage_volume_candidate.get_extent(storage_volume_candidate.free_space_core_slice)

## for debugging
                    if np.logical_and(np.logical_and(iFreeSpaceCore == iFreeSpaceCore_dbg, iAxis == iAxis_dbg), iAxis_value == iAxis_value_dbg):
                        vertices = np.asarray(get_bounding_box_verteces(storage_volume_candidate.extent), int)
                        vertices_in_grid = self.get_voxels_grid(vertices)

                        if bVisualization:
                            # self.visualize_voxels_model_and_ROI(vertices_in_grid)
                            voxels_slice_grid = self.get_voxels_grid(free_space_core_slice_layer_adjusted)
                            self.visualize_voxels_model_and_ROI_bbox(voxels_slice_grid, storage_volume_candidate.extent)
                        stop = 1
## end debugging

                    " Check if dimension of free space core bounding box is greater than threshold "
                    bbox_size = (
                        np.subtract(storage_volume_candidate.extent[:, 1], storage_volume_candidate.extent[:, 0]) + 1
                    )
                    if np.any(bbox_size < bbox_size_threshold):
                        continue
                    else:
                        " Check if free space core bounding box is supported by adequate flat surface "
                        if storage_volume_candidate.extent[1, 0] == 0:
                            # print("Free space core {} does not have supporting surface.".format(iFreeSpaceCore))
                            continue
                        else:
                            storage_volume_candidate.get_supporting_surface()

## for debugging
                            if np.logical_and(np.logical_and(iFreeSpaceCore == iFreeSpaceCore_dbg, iAxis == iAxis_dbg), iAxis_value == iAxis_value_dbg):
                                ROI = np.argwhere(storage_volume_candidate.supporting_layer == 1)
                                voxels_grid = self.get_voxels_grid_from_2D_ROI(
                                    ROI, storage_volume_candidate.supporting_surface_height
                                )
                               

                                if bVisualization:
                                    self.visualize_voxels_model_and_ROI(voxels_grid)
                                stop = 1
## end debugging

                            if (
                                storage_volume_candidate.supporting_surface_coverage
                                <= supporting_surface_coverage_threshold
                            ):
                                continue

                            adequate_supporting_surface = storage_volume_candidate.check_supporting_surface(
                                storage_volume_candidate.supporting_surface, bbox_size_threshold
                            )

                            if not adequate_supporting_surface:
                                stop = 1
                                continue

                            if storage_volume_candidate.supporting_surface_occupancy < (
                                bbox_size_threshold * bbox_size_threshold
                            ):
                                # print("Free space core {} has no adequate supporting surface.".format(iFreeSpaceCore))
                                stop = 1
                                continue

                            else:
                                " Adjust empty volume bbox extent to detected supporting surface"
                                storage_volume_candidate.get_storage_surface_core()
                                storage_volume_candidate.adjust_to_supporting_surface_v2()

                                " Check if free space core has odd number of (height) layers"
                                if len(np.unique(free_space_core_cells[:, 1])) % 2 == 0:
                                    if storage_volume_candidate.extent[1, 1] < self.size - 1:
                                        storage_volume_candidate.extent[1, 1] = (
                                            storage_volume_candidate.extent[1, 1] + 1
                                        )
                                        storage_volume_candidate.double_FreeSpaceCore_layer = True

                                storage_volume_candidate.vertices = get_bounding_box_verteces(
                                    storage_volume_candidate.extent
                                )

## foor debugging
                                if np.logical_and(np.logical_and(iFreeSpaceCore == iFreeSpaceCore_dbg, iAxis == iAxis_dbg), iAxis_value == iAxis_value_dbg):
                                    vertices_in_grid = self.get_voxels_grid(
                                        np.asarray(storage_volume_candidate.vertices, int)
                                    )
                                    # self.visualize_voxels_model_and_ROI(vertices_in_grid)

                                    ROI = np.argwhere(storage_volume_candidate.supporting_layer == 1)
                                    voxels_grid = self.get_voxels_grid_from_2D_ROI(
                                        ROI, storage_volume_candidate.supporting_surface_height
                                    )

                                    if bVisualization:
                                        self.visualize_voxels_model_and_ROI_bbox(
                                        voxels_grid, storage_volume_candidate.extent
                                        )

                                    stop = 1
## end debugging

                                bbox_size = (
                                    np.subtract(
                                        storage_volume_candidate.extent[:, 1], storage_volume_candidate.extent[:, 0]
                                    )
                                    + 1
                                )
                                if np.any(bbox_size < bbox_size_threshold):
                                    # print("Free space core {} bounding box is too small.".format(iFreeSpaceCore))
                                    continue
                                else:
                                    volume = (
                                        storage_volume_candidate.width
                                        * storage_volume_candidate.length
                                        * storage_volume_candidate.height
                                    )

                                    storage_volume_candidate.get_supporting_surface_bbox()

                                    if storage_volume_candidate.volume > volume0:
                                        volume0 = volume
                                        storage_volume_candidate.iFreeSpaceCore[0] = iFreeSpaceCore
                                        storage_volume_candidate.iFreeSpaceCore[1] = iAxis
                                        storage_volume_candidate.iFreeSpaceCore[2] = iAxis_value
                                        largest_bbox = storage_volume_candidate

                                    if np.logical_and(
                                        np.logical_and(iFreeSpaceCore == 5, iAxis == 0), iAxis_value == 8
                                    ):
                                        # storage_volume_candidate.visualize_bounding_box()
                                        stop = 1
                            stop = 1
                # end of iAxis_value

                if volume0 > 0:
                    storage_volumes.append(largest_bbox)
            # end of iAxis

            if not storage_volumes:
                continue
            else:
                volumes = [bbox.volume for bbox in storage_volumes]

                id = volumes.index(max(volumes))
                self.storage_volumes.append(storage_volumes[id])

                # if np.logical_and(
                #     np.logical_and(
                #         storage_volumes[id].iFreeSpaceCore[0] == 14, storage_volumes[id].iFreeSpaceCore[1] == 1
                #     ),
                #     storage_volumes[id].iFreeSpaceCore[2] == 9,
                # ):
                #     voxels_min_max_core = np.zeros([self.size, self.size, self.size])
                #     voxels_min_max_core[tuple(storage_volumes[id].storage_surface_core[:, 1])] = 1
                #     voxels_min_max_core[tuple(storage_volumes[id].storage_surface_core[:, 1])] = 1
                #     self.visualize_voxels_model_and_ROI(voxels_min_max_core)

                bVisualize_each = False
                if bVisualize_each:
                    storage_volumes[id].visualize_bounding_box()

                    surface_below_bbox_3D = np.zeros([self.size, self.size, self.size])
                    surface_below_bbox_3D[:, storage_volumes[id].extent[1, 0], :] = storage_volumes[id].supporting_layer
                    # self.visualize_voxels_model_and_ROI(surface_below_bbox_3D)

                    stop = 1

            stop = 1
        # end of iFreeSpaceCore
        
        if bVisualize_all:
            # surface_below_bbox_3D = np.zeros([self.size, self.size, self.size])
            # for bbox in self.storage_volumes:
            #     surface_below_bbox_3D[:, bbox.extent[1, 0], :] = bbox.supporting_layer
            # self.visualize_voxels_model_and_ROI(surface_below_bbox_3D)

            voxels_min_max_bb = np.zeros([self.size, self.size, self.size])
            for bbox in self.storage_volumes:
                for i in range(len(bbox.vertices)):
                    j, k, l = tuple(bbox.vertices[i])
                    j = int(j)
                    k = int(k)
                    l = int(l)
                    voxels_min_max_bb[j, k, l] = 1

            self.visualize_voxels_model_and_ROI(voxels_min_max_bb)

            stop = 1

    def get_bounding_boxes_old(self, bbox_size_threshold, supporting_surface_coverage_threshold, bVisualize=False):
        """ 
        ZINC 2021
        author={Hržica, Mateja and Durović, Petra and Džijan, Matej and Cupec, Robert},
        booktitle={2021 Zooming Innovation in Consumer Technologies Conference (ZINC)}, 
        title={Finding empty volumes inside 3D models of storage furniture}, 
     	year={2021},
        pages={242-245},
        doi={10.1109/ZINC52049.2021.9499292}}
        """
        for iFreeSpaceCore in range(1, np.max(self.free_space_cores) + 1):

            if iFreeSpaceCore == 6:
                stop = 1

            free_space_core_cells = np.argwhere(self.free_space_cores == iFreeSpaceCore)

            storage_volume_candidate = self._bbox(self)
            storage_volume_candidate.get_extent(free_space_core_cells)

            # top_shelf = (
            #     storage_volume_candidate.extent[1, 0]
            #     >= storage_volume_candidate.voxelized_model.size - bbox_size_threshold - 1
            # )

            top_shelf = (
                storage_volume_candidate.extent[1, :]
                >= storage_volume_candidate.voxelized_model.size - bbox_size_threshold - 1
            )

            if np.any(top_shelf):
                stop = 1

            " Check if dimension of free space core bounding box is greater than threshold "
            bbox_size = np.subtract(storage_volume_candidate.extent[:, 1], storage_volume_candidate.extent[:, 0]) + 1
            # if np.logical_and(np.any(bbox_size < bbox_size_threshold), not top_shelf):
            #     continue
            if np.any(bbox_size < bbox_size_threshold):
                continue

            else:
                " Check if free space core bounding box is supported by adequate flat surface "
                if storage_volume_candidate.extent[1, 0] == 0:
                    # print("Free space core {} does not have supporting surface.".format(iFreeSpaceCore))
                    continue
                # elif top_shelf:
                #     storage_volume_candidate.get_supporting_surface()
                #     if storage_volume_candidate.supporting_surface_coverage <= supporting_surface_coverage_threshold:
                #         # print("Free space core {} has no adequate supporting surface.".format(iFreeSpaceCore))
                #         continue
                #     else:
                #         storage_volume_candidate.get_storage_surface_core()
                #         storage_volume_candidate.adjust_to_supporting_surface()
                #         storage_volume_candidate.vertices = get_bounding_box_verteces(storage_volume_candidate.extent)
                #         storage_volume_candidate.iFreeSpaceCore = iFreeSpaceCore
                #         self.storage_volumes.append(storage_volume_candidate)

                #         voxels_local_max = np.zeros((self.size, self.size, self.size)).astype(np.int32)
                #         # distances = []
                #         for iComponent in range(len(free_space_core_cells)):
                #             xyz = tuple(free_space_core_cells[iComponent])
                #             voxels_local_max[xyz] = 1
                #             # distances.append(self.occupancy_distances[xyz])
                #         # self.visualize_voxels_model_and_ROI(voxels_local_max)

                #     stop = 1

                else:
                    storage_volume_candidate.get_supporting_surface()
                    if storage_volume_candidate.supporting_surface_coverage <= supporting_surface_coverage_threshold:
                        # print("Free space core {} has no adequate supporting surface.".format(iFreeSpaceCore))
                        continue
                    else:
                        " Adjust empty volume bbox extent to detected supporting surface"
                        if iFreeSpaceCore == 40:
                            stop = 1

                        storage_volume_candidate.get_storage_surface_core()

                        storage_volume_candidate.adjust_to_supporting_surface()

                        storage_volume_candidate.vertices = get_bounding_box_verteces(storage_volume_candidate.extent)

                        bbox_size = (
                            np.subtract(storage_volume_candidate.extent[:, 1], storage_volume_candidate.extent[:, 0])
                            + 1
                        )
                        if np.any(bbox_size < bbox_size_threshold):
                            # print("Free space core {} bounding box is too small.".format(iFreeSpaceCore))
                            continue
                        else:
                            storage_volume_candidate.iFreeSpaceCore = iFreeSpaceCore
                            self.storage_volumes.append(storage_volume_candidate)

                            # if iFreeSpaceCore < 40:
                            #     continue

                            if bVisualize:
                                self.visualize_voxels_model_and_ROI(self.distances_local_max)

                                voxels_local_max = np.zeros((self.size, self.size, self.size)).astype(np.int32)
                                distances = []
                                for iComponent in range(len(free_space_core_cells)):
                                    xyz = tuple(free_space_core_cells[iComponent])
                                    voxels_local_max[xyz] = 1
                                    distances.append(self.occupancy_distances[xyz])
                                self.visualize_voxels_model_and_ROI(voxels_local_max)

                                storage_volume_candidate.visualize_bounding_box()

                                surface_below_bbox_3D = np.zeros([self.size, self.size, self.size])
                                surface_below_bbox_3D[
                                    :, storage_volume_candidate.extent[1, 0], :
                                ] = storage_volume_candidate.supporting_layer
                                # self.visualize_voxels_model_and_ROI(surface_below_bbox_3D)
                                stop = 1

    def visualize_voxels_model_and_ROI(self, voxels_ROI):
        # Create plot
        fig = plt.figure(figsize=(10, 10), dpi=80)

        ax = fig.gca(projection=Axes3D.name)
        voxels_model = self.voxels.transpose(2, 0, 1)
        voxels_ROI = voxels_ROI.transpose(2, 0, 1)

        # ax.voxels(voxels_model, facecolors=[0.91, 0.93, 1, 0.85], edgecolors=[0.17, 0.18, 0.19, 0.85])
        # ax.voxels(voxels_model, facecolors=[0.82, 0.93, 0.95, 0.85], edgecolors=[0.17, 0.18, 0.19, 0.85])
        ax.voxels(voxels_model, facecolors=[0.67, 0.85, 0.9, 0.95], edgecolors=[0.17, 0.18, 0.19, 0.85])

        # ax.voxels(voxels_ROI, facecolors=[0, 0.5, 0.5], edgecolors="g")
        # ax.voxels(voxels_ROI, facecolors=[0.9, 0.4, 0], edgecolors="r")
        ax.voxels(voxels_ROI, facecolors="tab:orange", edgecolors=[0.9, 0.2, 0])
        # ax.voxels(voxels_ROI, facecolors=[1, 0, 1], edgecolors="k")

        # ax.voxels(voxels_model, edgecolor="k")
        # ax.voxels(voxels_ROI, edgecolor="red")

        # ax.set_xlabel("Z")
        # ax.set_ylabel("X")
        # ax.set_zlabel("Y")

        plt.axis("off")
        plt.grid(b=None)

        ax.view_init(elev=2, azim=76)
        plt.show()
        # plt.close(fig)

    def visualize_voxels_model_and_ROI_bbox(self, voxels_ROI, extent):
        # Create plot
        fig = plt.figure(figsize=(10, 10), dpi=80)

        ax = fig.gca(projection=Axes3D.name)
        voxels_model = self.voxels.transpose(2, 0, 1)
        voxels_ROI = voxels_ROI.transpose(2, 0, 1)

        ax.voxels(voxels_model, facecolors=[0.67, 0.85, 0.9, 1], edgecolors=[0.17, 0.18, 0.19], zorder=5)
        # ax.voxels(voxels_ROI, facecolors=[0.9, 0.4, 0], edgecolors="r", zorder=10)
        ax.voxels(voxels_ROI, facecolors=[0, 1, 0, 1], edgecolors="g", zorder=10)

        extent[:, 1] = extent[:, 1] + 1
        x = extent[2, :]
        y = extent[0, :]
        z = extent[1, :]

        xx, yy = np.meshgrid(x, y)

        z0 = np.ones_like(xx) * z[0]
        z1 = np.ones_like(xx) * z[1]

        a = 0.1
        ord = 0
        bc = [1, 0.5, 0]

        ax.plot_wireframe(xx, yy, z0, color=bc, zorder=ord)
        ax.plot_surface(xx, yy, z0, color=bc, alpha=a, zorder=ord)
        ax.plot_wireframe(xx, yy, z1, color=bc, zorder=ord)
        ax.plot_surface(xx, yy, z1, color=bc, alpha=a, zorder=ord)

        # ax.plot_wireframe(xx, yy, z0, color="r", zorder=ord)
        # ax.plot_surface(xx, yy, z0, color="r", alpha=a, zorder=ord)
        # ax.plot_wireframe(xx, yy, z1, color="r", zorder=ord)
        # ax.plot_surface(xx, yy, z1, color="r", alpha=a, zorder=ord)

        yy, zz = np.meshgrid(y, z)
        ax.plot_wireframe(x[0], yy, zz, color=bc, zorder=ord)
        ax.plot_surface(x[0], yy, zz, color=bc, alpha=a, zorder=ord)
        ax.plot_wireframe(x[1], yy, zz, color=bc, zorder=ord)
        ax.plot_surface(x[1], yy, zz, color=bc, alpha=a, zorder=ord)

        xx, zz = np.meshgrid(x, z)
        ax.plot_wireframe(xx, y[0], zz, color=bc, zorder=ord)
        ax.plot_surface(xx, y[0], zz, color=bc, alpha=a, zorder=ord)
        ax.plot_wireframe(xx, y[1], zz, color=bc, zorder=ord)
        ax.plot_surface(xx, y[1], zz, color=bc, alpha=a, zorder=ord)

        plt.axis("off")
        plt.grid(b=None)

        ax.view_init(elev=2, azim=76)
        plt.show()
        # plt.close(fig)

    def visualize_voxels_model_and_2D_ROI(self, ROI_2D, height):
        voxels_grid = np.zeros([self.size, self.size, self.size])
        for voxel in ROI_2D:
            j, l = tuple(voxel)
            j = int(j)
            k = int(height)
            l = int(l)
            voxels_grid[j, k, l] = 1

        self.visualize_voxels_model_and_ROI(voxels_grid)

    def createInner(self):
        return BinvoxModel._bbox(self)

    class _bbox:
        def __init__(self, voxelized_model):

            self.voxelized_model = voxelized_model
            self.extent = np.ones((3, 2)).astype(np.int32) * (-1)

            self.vertices = np.empty((8, 3))

            self.supporting_layer = np.zeros([self.voxelized_model.size, self.voxelized_model.size])

            self.supporting_surface = {}
            self.supporting_surface_height = {}
            self.supporting_surface_occupancy = {}
            self.supporting_surface_coverage = {}

            self.storage_surface_core = np.ones((2, 2)).astype(np.int32) * (-1)
            self.supporting_surface_vertices = {}

            self.width = 0
            self.length = 0
            self.height = 0
            self.volume = 0

            self.iFreeSpaceCore = {}
            self.free_space_core_slice = {}
            self.double_FreeSpaceCore_layer = False

        def get_extent(self, free_space_core_cells):
            axis_boundry_min = np.any(free_space_core_cells == 0, axis=0)
            axis_boundry_max = np.any(free_space_core_cells == self.voxelized_model.size - 1, axis=0)

            core_min_coords = tuple(np.amin(free_space_core_cells, axis=0))
            core_max_coords = tuple(np.amax(free_space_core_cells, axis=0))

            # proba
            coord = tuple(free_space_core_cells[0])
            self.extent[:, 0] = (
                core_min_coords - (self.voxelized_model.occupancy_distances[coord] - 1) * ~axis_boundry_min
            )

            if np.any(self.extent[:, 0] < 0):
                self.extent[:, 0] = np.where(self.extent[:, 0] < 0, 0, self.extent[:, 0])

            self.extent[:, 1] = (
                core_max_coords + (self.voxelized_model.occupancy_distances[coord] - 1) * ~axis_boundry_max
            )

            if np.any(self.extent[:, 1] >= self.voxelized_model.size):
                self.extent[:, 1] = np.where(
                    self.extent[:, 1] >= self.voxelized_model.size, (self.voxelized_model.size - 1), self.extent[:, 1]
                )

        def get_dimensions(self):
            self.width = self.extent[0, 1] - self.extent[0, 0] + 1
            self.height = self.extent[1, 1] - self.extent[1, 0] + 1
            self.length = self.extent[2, 1] - self.extent[2, 0] + 1

            self.volume = self.width * self.height * self.length

        def get_supporting_surface(self):
            self.get_dimensions()

            self.supporting_surface_height = self.extent[1, 0] - 1

            self.supporting_surface = self.voxelized_model.voxels[
                self.extent[0, 0] : (self.extent[0, 1] + 1),
                self.supporting_surface_height,
                self.extent[2, 0] : (self.extent[2, 1] + 1),
            ]

            self.supporting_layer[
                self.extent[0, 0] : (self.extent[0, 1] + 1), self.extent[2, 0] : (self.extent[2, 1] + 1),
            ] = self.supporting_surface

            self.supporting_surface_occupancy = np.count_nonzero(self.supporting_surface)
            bb_surface = self.length * self.width

            self.supporting_surface_coverage = self.supporting_surface_occupancy / bb_surface

        def check_supporting_surface(self, supporting_surface, bbox_size_threshold):

            # create a padded copy of supporting surface
            supporting_surface_int = np.copy(supporting_surface.astype(int))
            border_width = 1
            supporting_surface_padded = np.pad(supporting_surface_int, border_width, "constant")

            # size = bbox_size_threshold --> is actually bbox_size_threshold x bbox_size_threshold
            supporting_surface_filtered = sc.ndimage.uniform_filter(
                supporting_surface_padded, size=bbox_size_threshold, output=None, mode="nearest", cval=0.0, origin=0
            )

            if np.any(supporting_surface_filtered == 1):
                stop = 1
                supporting_surface_layer = np.pad(
                    self.supporting_layer, pad_width=1, mode="constant", constant_values=0
                )

                supporting_surface_layer = supporting_surface_layer.astype(int)

                connectivity = 4
                connected_components = self.voxelized_model.get_connected_components(
                    supporting_surface_layer, connectivity
                )

                if np.sum(connected_components) == 0:
                    stop = 1

                if connected_components.max() < 2:
                    return True
                else:
                    stop = 1
                    return False

        def get_storage_surface_core(self):
            try:
                self.occupancy_distances = self.get_2D_distance_transform()
                max_distance = np.max(self.occupancy_distances)
                local_maxs = np.where(self.occupancy_distances == max_distance, 1, 0)

                distances_local_max_ = np.where(local_maxs == 1)
                self.distances_local_max = list(zip(distances_local_max_[0], distances_local_max_[1]))
                stop = 1

                self.storage_surface_core[:, 0] = np.amin(self.distances_local_max, axis=0)
                self.storage_surface_core[:, 1] = np.amax(self.distances_local_max, axis=0)
                return True
            except:
                pass

        def dilate_layer(self, layer, fill_with):
            dilate_size = 1
            grid_size = self.voxelized_model.size + dilate_size * 2
            layer_dilated = np.ones((grid_size, grid_size)) * fill_with
            layer_dilated[dilate_size : grid_size - dilate_size, dilate_size : grid_size - dilate_size] = layer

            stop = 1

            return layer_dilated

        def shrink_layer(self, layer):
            dilate_size = 1
            grid_size = self.voxelized_model.size + dilate_size * 2
            shrink_layer = layer[dilate_size : grid_size - dilate_size, dilate_size : grid_size - dilate_size]

            stop = 1

            return shrink_layer

        def get_2D_distance_transform(self):
            return sc.ndimage.distance_transform_cdt(
                self.supporting_layer,
                metric="chessboard",
                return_distances=True,
                return_indices=False,
                distances=None,
                indices=None,
            )

        def adjust_to_supporting_surface(self):
            # Get minimal coordiantes.
            self.extent[0, 0] = max(
                self.storage_surface_core[0, 0] - self.occupancy_distances[tuple(self.storage_surface_core[:, 0])], 0,
            )

            if self.extent[1, 0] > 0:
                self.extent[1, 0] = self.extent[1, 0] - 1
            else:
                self.extent[1, 0] = self.extent[1, 0]

            self.extent[2, 0] = max(
                self.storage_surface_core[1, 0] - self.occupancy_distances[tuple(self.storage_surface_core[:, 0])], 0,
            )

            # Get maximal coordinates.
            self.extent[0, 1] = min(
                self.storage_surface_core[0, 1] + self.occupancy_distances[tuple(self.storage_surface_core[:, 1])],
                self.voxelized_model.size - 1,
            )

            if self.extent[1, 1] < self.voxelized_model.size - 1:
                self.extent[1, 1] = self.extent[1, 1] + 1
            else:
                self.extent[1, 1] = self.extent[1, 1]

            self.extent[2, 1] = min(
                self.storage_surface_core[1, 1] + self.occupancy_distances[tuple(self.storage_surface_core[:, 1])],
                self.voxelized_model.size - 1,
            )

        def adjust_to_supporting_surface_v2(self):
            # Get minimal coordiantes.
            self.extent[0, 0] = max(
                self.storage_surface_core[0, 0]
                - (self.occupancy_distances[tuple(self.storage_surface_core[:, 0])] - 1),
                0,
            )

            self.extent[2, 0] = max(
                self.storage_surface_core[1, 0]
                - (self.occupancy_distances[tuple(self.storage_surface_core[:, 0])] - 1),
                0,
            )

            # Get maximal coordinates.
            self.extent[0, 1] = min(
                self.storage_surface_core[0, 1]
                + (self.occupancy_distances[tuple(self.storage_surface_core[:, 1])] - 1),
                self.voxelized_model.size - 1,
            )

            self.extent[2, 1] = min(
                self.storage_surface_core[1, 1]
                + (self.occupancy_distances[tuple(self.storage_surface_core[:, 1])] - 1),
                self.voxelized_model.size - 1,
            )

        def get_supporting_surface_bbox(self):
            supporting_surface_bbox = np.copy(self.extent).astype(float)
            supporting_surface_bbox[1, 1] = supporting_surface_bbox[1, 0] * 1.00001

            self.supporting_surface_vertices = get_bounding_box_verteces(supporting_surface_bbox)

        def visualize_bounding_box(self):
            """Visualize voxelized bounding box edge points with matplotlib.

                Parameters
                ---------
                bounding_box_coords : ndarray, shape(3, 2) 
                    cooridnates (xyz) are written in rows
                    first column contains minimal coordinates, second column contains maximal coordinates
                voxels_model: 3d ndarray of voxelized model

                Returns
                -------
                visualization window
            """

            voxels_min_max_bb = np.zeros(
                [self.voxelized_model.size, self.voxelized_model.size, self.voxelized_model.size]
            )
            for i in range(len(self.vertices)):
                j, k, l = tuple(self.vertices[i])
                j = int(j)
                k = int(k)
                l = int(l)
                voxels_min_max_bb[j, k, l] = 1

            self.voxelized_model.visualize_voxels_model_and_ROI(voxels_min_max_bb)

