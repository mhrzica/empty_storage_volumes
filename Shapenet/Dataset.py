import os


class Dataset(object):
    """
    
    Loads dataset.
    
    root_dir:
        Path to datasets root directory. This folder should contain folders which are named as dataset classes. 
        In each class folder there should be folders: "4_watertight_scaled" and "5_voxels"
        If you want to save bboxes to file also create folders: "6_bounding_boxes" and "7_images"

        Folder "root_dir/class_dir/4_watertight_scaled" shold contain ONLY CAD mesh models of furniture in *.off format
        Folder "root_dir/class_dir/5_voxels" shold contain ONLY voxelized furniture models in *.binvox format
    
    model_class:
        Current class which should be loaded.

    """

    def __init__(self, root_dir, model_class):
        self.root_dir = root_dir

        self.class_dir = os.path.join(self.root_dir, model_class)
        self.watertight_scaled_dir = os.path.join(self.class_dir, "4_watertight_scaled")

        self.binvox_dir = os.path.join(self.class_dir, "5_voxels")

        self.bboxes_dir = os.path.join(self.class_dir, "6_bounding_boxes")

        self.visualization_dir = os.path.join(self.class_dir, "7_images")

        self.models_list = [x[:-4] for x in os.listdir(self.watertight_scaled_dir)]

    def get_voxelized_model_path(self, model_name):
        model_filepath = os.path.join(self.binvox_dir, "%s.binvox" % (model_name))
        return model_filepath

    def get_mesh_model_path(self, model_name):
        model_filepath = os.path.join(self.watertight_scaled_dir, "%s.off" % (model_name))
        return model_filepath

    def get_bbox_save_path(self, model_name):
        model_filepath = os.path.join(self.bboxes_dir, model_name)

        if os.path.isdir(model_filepath):
            return model_filepath
        else:
            os.mkdir(model_filepath)
            return model_filepath

    def get_visualization_path(self, model_name):
        model_filepath = os.path.join(self.visualization_dir, "%s.png" % (model_name))
        return model_filepath
