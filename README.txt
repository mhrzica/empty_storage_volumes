Python 3.6 with packages listed in "requirements.txt"

Push the "requirements.txt" file to anywhere you want to deploy the code, and then type in terminal:

$ virtualenv <env_name>
$ source <env_name>/bin/activate
(<env_name>)$ pip install -r path/to/requirements.txt
